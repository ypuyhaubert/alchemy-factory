﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.Char[]
struct CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t4A961510635950236678F1B3B2436ECCAF28713A;
// System.Collections.Generic.List`1<System.Single>
struct List_1_tC02C2993D5A6DDB73B1126E4EECDEB641C54A03E;
// System.Object[]
struct ObjectU5BU5D_t8D571697F3A1B33B696E2F80500C21F1A1748C5D;
// System.String
struct String_t;
// System.Void
struct Void_tDB81A15FA2AB53E2401A76B745D215397B29F783;
// TMPro.Examples.TextMeshProFloatingText
struct TextMeshProFloatingText_tD52C5B081FC480F24349C2C0F5099F0693EEC913;
// TMPro.Examples.VertexColorCycler
struct VertexColorCycler_t5EEC79CC79AA4B00DF4BE5CE390C12E47385299C;
// TMPro.Examples.VertexJitter
struct VertexJitter_tF4F1AE4AF51C09E6FE57F91227A228B61BD0ACF0;
// TMPro.Examples.VertexJitter/VertexAnim[]
struct VertexAnimU5BU5D_tFDF75428176C5B23AB66CAAECB9202F55DA6E13F;
// TMPro.Examples.VertexShakeA
struct VertexShakeA_tCBD5B7397DD332038CE795E6B0A4D5B199739C27;
// TMPro.Examples.VertexShakeB
struct VertexShakeB_tC2BBE39E80035E065C9ADB888AA52006D10EDC0E;
// TMPro.Examples.VertexZoom
struct VertexZoom_t78EE9E8C38B64E198806ADD562E22F722A2EE156;
// TMPro.Examples.VertexZoom/<AnimateVertexColors>c__Iterator0
struct U3CAnimateVertexColorsU3Ec__Iterator0_t4CE04A71641C727D7CFD708F629B27504B50CE7D;
// TMPro.Examples.VertexZoom/<AnimateVertexColors>c__Iterator0/<AnimateVertexColors>c__AnonStorey1
struct U3CAnimateVertexColorsU3Ec__AnonStorey1_t22561508C8B53D4F45837DFC591B58212EEB80D6;
// TMPro.Examples.WarpTextExample
struct WarpTextExample_t918FAC4E8BDAF99FAE70218E452EB4EF81A1C9F2;
// TMPro.TMP_MeshInfo[]
struct TMP_MeshInfoU5BU5D_t7F7564862ADABD75DAD9B09FF274591F807FFDE9;
// TMPro.TMP_Text
struct TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7;
// TMPro.TMP_TextEventHandler
struct TMP_TextEventHandler_t6046295E0F06C5138916541DBE6B476E6FE66B54;
// TMPro.TMP_TextEventHandler/CharacterSelectionEvent
struct CharacterSelectionEvent_t346DE6835B0DF86A32928016EE2C413E919DBF90;
// TMPro.TMP_TextEventHandler/LineSelectionEvent
struct LineSelectionEvent_t9F223C373E9EF88E12570D86DE2651F8EDEEAB3F;
// TMPro.TMP_TextEventHandler/LinkSelectionEvent
struct LinkSelectionEvent_t61E2583194386CC362B13606ECE2F840876A24E8;
// TMPro.TMP_TextEventHandler/SpriteSelectionEvent
struct SpriteSelectionEvent_tCE8FEB1D487ED84CBA38BC47F8949C5537672B32;
// TMPro.TMP_TextEventHandler/WordSelectionEvent
struct WordSelectionEvent_tF37108E717AF8FCEB63C4B4CB11231A5F030A554;
// TMPro.TMP_TextInfo
struct TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181;
// TMPro.TextContainer
struct TextContainer_tF5E5EB56D152102B19C27607F84847CA594391DE;
// TMPro.TextMeshPro
struct TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2;
// TMPro.TextMeshProUGUI
struct TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438;
// UnityEngine.AnimationCurve
struct AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C;
// UnityEngine.Camera
struct Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34;
// UnityEngine.Canvas
struct Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591;
// UnityEngine.Color32[]
struct Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F;
// UnityEngine.Font
struct Font_t1EDE54AF557272BE314EB4B40EFA50CEB353CA26;
// UnityEngine.RectTransform
struct RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20;
// UnityEngine.Transform
struct Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28;
// UnityEngine.Vector3[][]
struct Vector3U5BU5DU5BU5D_tB83B3D7B4B4D7AD0A199DD45DF2963AB05A519AC;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef VALUETYPE_T1810BD84E0FDB5D3A7CD34286A5B22F343995C9C_H
#define VALUETYPE_T1810BD84E0FDB5D3A7CD34286A5B22F343995C9C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t1810BD84E0FDB5D3A7CD34286A5B22F343995C9C  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t1810BD84E0FDB5D3A7CD34286A5B22F343995C9C_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t1810BD84E0FDB5D3A7CD34286A5B22F343995C9C_marshaled_com
{
};
#endif // VALUETYPE_T1810BD84E0FDB5D3A7CD34286A5B22F343995C9C_H
#ifndef U3CANIMATEVERTEXCOLORSU3EC__ANONSTOREY1_T22561508C8B53D4F45837DFC591B58212EEB80D6_H
#define U3CANIMATEVERTEXCOLORSU3EC__ANONSTOREY1_T22561508C8B53D4F45837DFC591B58212EEB80D6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.VertexZoom/<AnimateVertexColors>c__Iterator0/<AnimateVertexColors>c__AnonStorey1
struct  U3CAnimateVertexColorsU3Ec__AnonStorey1_t22561508C8B53D4F45837DFC591B58212EEB80D6  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<System.Single> TMPro.Examples.VertexZoom/<AnimateVertexColors>c__Iterator0/<AnimateVertexColors>c__AnonStorey1::modifiedCharScale
	List_1_tC02C2993D5A6DDB73B1126E4EECDEB641C54A03E * ___modifiedCharScale_0;
	// TMPro.Examples.VertexZoom/<AnimateVertexColors>c__Iterator0 TMPro.Examples.VertexZoom/<AnimateVertexColors>c__Iterator0/<AnimateVertexColors>c__AnonStorey1::<>f__ref$0
	U3CAnimateVertexColorsU3Ec__Iterator0_t4CE04A71641C727D7CFD708F629B27504B50CE7D * ___U3CU3Ef__refU240_1;

public:
	inline static int32_t get_offset_of_modifiedCharScale_0() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__AnonStorey1_t22561508C8B53D4F45837DFC591B58212EEB80D6, ___modifiedCharScale_0)); }
	inline List_1_tC02C2993D5A6DDB73B1126E4EECDEB641C54A03E * get_modifiedCharScale_0() const { return ___modifiedCharScale_0; }
	inline List_1_tC02C2993D5A6DDB73B1126E4EECDEB641C54A03E ** get_address_of_modifiedCharScale_0() { return &___modifiedCharScale_0; }
	inline void set_modifiedCharScale_0(List_1_tC02C2993D5A6DDB73B1126E4EECDEB641C54A03E * value)
	{
		___modifiedCharScale_0 = value;
		Il2CppCodeGenWriteBarrier((&___modifiedCharScale_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU240_1() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__AnonStorey1_t22561508C8B53D4F45837DFC591B58212EEB80D6, ___U3CU3Ef__refU240_1)); }
	inline U3CAnimateVertexColorsU3Ec__Iterator0_t4CE04A71641C727D7CFD708F629B27504B50CE7D * get_U3CU3Ef__refU240_1() const { return ___U3CU3Ef__refU240_1; }
	inline U3CAnimateVertexColorsU3Ec__Iterator0_t4CE04A71641C727D7CFD708F629B27504B50CE7D ** get_address_of_U3CU3Ef__refU240_1() { return &___U3CU3Ef__refU240_1; }
	inline void set_U3CU3Ef__refU240_1(U3CAnimateVertexColorsU3Ec__Iterator0_t4CE04A71641C727D7CFD708F629B27504B50CE7D * value)
	{
		___U3CU3Ef__refU240_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__refU240_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CANIMATEVERTEXCOLORSU3EC__ANONSTOREY1_T22561508C8B53D4F45837DFC591B58212EEB80D6_H
#ifndef UNITYEVENTBASE_T6E0F7823762EE94BB8489B5AE41C7802A266D3D5_H
#define UNITYEVENTBASE_T6E0F7823762EE94BB8489B5AE41C7802A266D3D5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEventBase
struct  UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * ___m_PersistentCalls_1;
	// System.String UnityEngine.Events.UnityEventBase::m_TypeName
	String_t* ___m_TypeName_2;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_3;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_Calls_0)); }
	inline InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Calls_0), value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PersistentCalls_1), value);
	}

	inline static int32_t get_offset_of_m_TypeName_2() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_TypeName_2)); }
	inline String_t* get_m_TypeName_2() const { return ___m_TypeName_2; }
	inline String_t** get_address_of_m_TypeName_2() { return &___m_TypeName_2; }
	inline void set_m_TypeName_2(String_t* value)
	{
		___m_TypeName_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_TypeName_2), value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_3() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_CallsDirty_3)); }
	inline bool get_m_CallsDirty_3() const { return ___m_CallsDirty_3; }
	inline bool* get_address_of_m_CallsDirty_3() { return &___m_CallsDirty_3; }
	inline void set_m_CallsDirty_3(bool value)
	{
		___m_CallsDirty_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENTBASE_T6E0F7823762EE94BB8489B5AE41C7802A266D3D5_H
#ifndef ENUM_T5AAC444DFCAA78411386665A25FE3CD3169879EF_H
#define ENUM_T5AAC444DFCAA78411386665A25FE3CD3169879EF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t5AAC444DFCAA78411386665A25FE3CD3169879EF  : public ValueType_t1810BD84E0FDB5D3A7CD34286A5B22F343995C9C
{
public:

public:
};

struct Enum_t5AAC444DFCAA78411386665A25FE3CD3169879EF_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t5AAC444DFCAA78411386665A25FE3CD3169879EF_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t5AAC444DFCAA78411386665A25FE3CD3169879EF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t5AAC444DFCAA78411386665A25FE3CD3169879EF_marshaled_com
{
};
#endif // ENUM_T5AAC444DFCAA78411386665A25FE3CD3169879EF_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef VERTEXANIM_T8FDDCAB8C8E4CDA915AACE07F8ACD16108BEA61C_H
#define VERTEXANIM_T8FDDCAB8C8E4CDA915AACE07F8ACD16108BEA61C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.VertexJitter/VertexAnim
struct  VertexAnim_t8FDDCAB8C8E4CDA915AACE07F8ACD16108BEA61C 
{
public:
	// System.Single TMPro.Examples.VertexJitter/VertexAnim::angleRange
	float ___angleRange_0;
	// System.Single TMPro.Examples.VertexJitter/VertexAnim::angle
	float ___angle_1;
	// System.Single TMPro.Examples.VertexJitter/VertexAnim::speed
	float ___speed_2;

public:
	inline static int32_t get_offset_of_angleRange_0() { return static_cast<int32_t>(offsetof(VertexAnim_t8FDDCAB8C8E4CDA915AACE07F8ACD16108BEA61C, ___angleRange_0)); }
	inline float get_angleRange_0() const { return ___angleRange_0; }
	inline float* get_address_of_angleRange_0() { return &___angleRange_0; }
	inline void set_angleRange_0(float value)
	{
		___angleRange_0 = value;
	}

	inline static int32_t get_offset_of_angle_1() { return static_cast<int32_t>(offsetof(VertexAnim_t8FDDCAB8C8E4CDA915AACE07F8ACD16108BEA61C, ___angle_1)); }
	inline float get_angle_1() const { return ___angle_1; }
	inline float* get_address_of_angle_1() { return &___angle_1; }
	inline void set_angle_1(float value)
	{
		___angle_1 = value;
	}

	inline static int32_t get_offset_of_speed_2() { return static_cast<int32_t>(offsetof(VertexAnim_t8FDDCAB8C8E4CDA915AACE07F8ACD16108BEA61C, ___speed_2)); }
	inline float get_speed_2() const { return ___speed_2; }
	inline float* get_address_of_speed_2() { return &___speed_2; }
	inline void set_speed_2(float value)
	{
		___speed_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTEXANIM_T8FDDCAB8C8E4CDA915AACE07F8ACD16108BEA61C_H
#ifndef COLOR32_T23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23_H
#define COLOR32_T23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color32
struct  Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 
{
public:
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Int32 UnityEngine.Color32::rgba
			int32_t ___rgba_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___rgba_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Byte UnityEngine.Color32::r
			uint8_t ___r_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint8_t ___r_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___g_2_OffsetPadding[1];
			// System.Byte UnityEngine.Color32::g
			uint8_t ___g_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___g_2_OffsetPadding_forAlignmentOnly[1];
			uint8_t ___g_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___b_3_OffsetPadding[2];
			// System.Byte UnityEngine.Color32::b
			uint8_t ___b_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___b_3_OffsetPadding_forAlignmentOnly[2];
			uint8_t ___b_3_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___a_4_OffsetPadding[3];
			// System.Byte UnityEngine.Color32::a
			uint8_t ___a_4;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___a_4_OffsetPadding_forAlignmentOnly[3];
			uint8_t ___a_4_forAlignmentOnly;
		};
	};

public:
	inline static int32_t get_offset_of_rgba_0() { return static_cast<int32_t>(offsetof(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23, ___rgba_0)); }
	inline int32_t get_rgba_0() const { return ___rgba_0; }
	inline int32_t* get_address_of_rgba_0() { return &___rgba_0; }
	inline void set_rgba_0(int32_t value)
	{
		___rgba_0 = value;
	}

	inline static int32_t get_offset_of_r_1() { return static_cast<int32_t>(offsetof(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23, ___r_1)); }
	inline uint8_t get_r_1() const { return ___r_1; }
	inline uint8_t* get_address_of_r_1() { return &___r_1; }
	inline void set_r_1(uint8_t value)
	{
		___r_1 = value;
	}

	inline static int32_t get_offset_of_g_2() { return static_cast<int32_t>(offsetof(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23, ___g_2)); }
	inline uint8_t get_g_2() const { return ___g_2; }
	inline uint8_t* get_address_of_g_2() { return &___g_2; }
	inline void set_g_2(uint8_t value)
	{
		___g_2 = value;
	}

	inline static int32_t get_offset_of_b_3() { return static_cast<int32_t>(offsetof(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23, ___b_3)); }
	inline uint8_t get_b_3() const { return ___b_3; }
	inline uint8_t* get_address_of_b_3() { return &___b_3; }
	inline void set_b_3(uint8_t value)
	{
		___b_3 = value;
	}

	inline static int32_t get_offset_of_a_4() { return static_cast<int32_t>(offsetof(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23, ___a_4)); }
	inline uint8_t get_a_4() const { return ___a_4; }
	inline uint8_t* get_address_of_a_4() { return &___a_4; }
	inline void set_a_4(uint8_t value)
	{
		___a_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR32_T23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23_H
#ifndef UNITYEVENT_2_T847360AB318A8D3485A7F2D074D4488D2132FA93_H
#define UNITYEVENT_2_T847360AB318A8D3485A7F2D074D4488D2132FA93_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`2<System.Char,System.Int32>
struct  UnityEvent_2_t847360AB318A8D3485A7F2D074D4488D2132FA93  : public UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`2::m_InvokeArray
	ObjectU5BU5D_t8D571697F3A1B33B696E2F80500C21F1A1748C5D* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_2_t847360AB318A8D3485A7F2D074D4488D2132FA93, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t8D571697F3A1B33B696E2F80500C21F1A1748C5D* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t8D571697F3A1B33B696E2F80500C21F1A1748C5D** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t8D571697F3A1B33B696E2F80500C21F1A1748C5D* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_2_T847360AB318A8D3485A7F2D074D4488D2132FA93_H
#ifndef UNITYEVENT_3_TF6326B92F2348C19417549314692990FA2279EAA_H
#define UNITYEVENT_3_TF6326B92F2348C19417549314692990FA2279EAA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`3<System.String,System.Int32,System.Int32>
struct  UnityEvent_3_tF6326B92F2348C19417549314692990FA2279EAA  : public UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`3::m_InvokeArray
	ObjectU5BU5D_t8D571697F3A1B33B696E2F80500C21F1A1748C5D* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_3_tF6326B92F2348C19417549314692990FA2279EAA, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t8D571697F3A1B33B696E2F80500C21F1A1748C5D* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t8D571697F3A1B33B696E2F80500C21F1A1748C5D** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t8D571697F3A1B33B696E2F80500C21F1A1748C5D* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_3_TF6326B92F2348C19417549314692990FA2279EAA_H
#ifndef UNITYEVENT_3_TEAC12FDE9BD5A3DEBA11C8302B47F0BD504043A9_H
#define UNITYEVENT_3_TEAC12FDE9BD5A3DEBA11C8302B47F0BD504043A9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`3<System.String,System.String,System.Int32>
struct  UnityEvent_3_tEAC12FDE9BD5A3DEBA11C8302B47F0BD504043A9  : public UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`3::m_InvokeArray
	ObjectU5BU5D_t8D571697F3A1B33B696E2F80500C21F1A1748C5D* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_3_tEAC12FDE9BD5A3DEBA11C8302B47F0BD504043A9, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t8D571697F3A1B33B696E2F80500C21F1A1748C5D* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t8D571697F3A1B33B696E2F80500C21F1A1748C5D** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t8D571697F3A1B33B696E2F80500C21F1A1748C5D* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_3_TEAC12FDE9BD5A3DEBA11C8302B47F0BD504043A9_H
#ifndef MATRIX4X4_T6BF60F70C9169DF14C9D2577672A44224B236ECA_H
#define MATRIX4X4_T6BF60F70C9169DF14C9D2577672A44224B236ECA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Matrix4x4
struct  Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  value)
	{
		___identityMatrix_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIX4X4_T6BF60F70C9169DF14C9D2577672A44224B236ECA_H
#ifndef OBJECTTYPE_T4A9206B564B1134F55E176CB1D6A831B7F2CD0BE_H
#define OBJECTTYPE_T4A9206B564B1134F55E176CB1D6A831B7F2CD0BE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TMP_ExampleScript_01/objectType
struct  objectType_t4A9206B564B1134F55E176CB1D6A831B7F2CD0BE 
{
public:
	// System.Int32 TMPro.Examples.TMP_ExampleScript_01/objectType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(objectType_t4A9206B564B1134F55E176CB1D6A831B7F2CD0BE, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTTYPE_T4A9206B564B1134F55E176CB1D6A831B7F2CD0BE_H
#ifndef FPSCOUNTERANCHORPOSITIONS_TF97255C7539822AFCA3511F3C0C4E6803A11EA97_H
#define FPSCOUNTERANCHORPOSITIONS_TF97255C7539822AFCA3511F3C0C4E6803A11EA97_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TMP_FrameRateCounter/FpsCounterAnchorPositions
struct  FpsCounterAnchorPositions_tF97255C7539822AFCA3511F3C0C4E6803A11EA97 
{
public:
	// System.Int32 TMPro.Examples.TMP_FrameRateCounter/FpsCounterAnchorPositions::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FpsCounterAnchorPositions_tF97255C7539822AFCA3511F3C0C4E6803A11EA97, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FPSCOUNTERANCHORPOSITIONS_TF97255C7539822AFCA3511F3C0C4E6803A11EA97_H
#ifndef FPSCOUNTERANCHORPOSITIONS_T4FFCAA8E98FD34C931C6524BB63FBE9828B9FBD0_H
#define FPSCOUNTERANCHORPOSITIONS_T4FFCAA8E98FD34C931C6524BB63FBE9828B9FBD0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TMP_UiFrameRateCounter/FpsCounterAnchorPositions
struct  FpsCounterAnchorPositions_t4FFCAA8E98FD34C931C6524BB63FBE9828B9FBD0 
{
public:
	// System.Int32 TMPro.Examples.TMP_UiFrameRateCounter/FpsCounterAnchorPositions::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FpsCounterAnchorPositions_t4FFCAA8E98FD34C931C6524BB63FBE9828B9FBD0, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FPSCOUNTERANCHORPOSITIONS_T4FFCAA8E98FD34C931C6524BB63FBE9828B9FBD0_H
#ifndef FPSCOUNTERANCHORPOSITIONS_TB558896962FD344179F43905F951BF8A256B8642_H
#define FPSCOUNTERANCHORPOSITIONS_TB558896962FD344179F43905F951BF8A256B8642_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TMPro_InstructionOverlay/FpsCounterAnchorPositions
struct  FpsCounterAnchorPositions_tB558896962FD344179F43905F951BF8A256B8642 
{
public:
	// System.Int32 TMPro.Examples.TMPro_InstructionOverlay/FpsCounterAnchorPositions::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FpsCounterAnchorPositions_tB558896962FD344179F43905F951BF8A256B8642, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FPSCOUNTERANCHORPOSITIONS_TB558896962FD344179F43905F951BF8A256B8642_H
#ifndef U3CANIMATEVERTEXCOLORSU3EC__ITERATOR0_T7BBBF1D9BED81210183303D57EA4AEE43847D4BA_H
#define U3CANIMATEVERTEXCOLORSU3EC__ITERATOR0_T7BBBF1D9BED81210183303D57EA4AEE43847D4BA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.VertexColorCycler/<AnimateVertexColors>c__Iterator0
struct  U3CAnimateVertexColorsU3Ec__Iterator0_t7BBBF1D9BED81210183303D57EA4AEE43847D4BA  : public RuntimeObject
{
public:
	// TMPro.TMP_TextInfo TMPro.Examples.VertexColorCycler/<AnimateVertexColors>c__Iterator0::<textInfo>__0
	TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181 * ___U3CtextInfoU3E__0_0;
	// System.Int32 TMPro.Examples.VertexColorCycler/<AnimateVertexColors>c__Iterator0::<currentCharacter>__0
	int32_t ___U3CcurrentCharacterU3E__0_1;
	// UnityEngine.Color32 TMPro.Examples.VertexColorCycler/<AnimateVertexColors>c__Iterator0::<c0>__0
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___U3Cc0U3E__0_2;
	// System.Int32 TMPro.Examples.VertexColorCycler/<AnimateVertexColors>c__Iterator0::<characterCount>__1
	int32_t ___U3CcharacterCountU3E__1_3;
	// System.Int32 TMPro.Examples.VertexColorCycler/<AnimateVertexColors>c__Iterator0::<materialIndex>__1
	int32_t ___U3CmaterialIndexU3E__1_4;
	// UnityEngine.Color32[] TMPro.Examples.VertexColorCycler/<AnimateVertexColors>c__Iterator0::<newVertexColors>__1
	Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983* ___U3CnewVertexColorsU3E__1_5;
	// System.Int32 TMPro.Examples.VertexColorCycler/<AnimateVertexColors>c__Iterator0::<vertexIndex>__1
	int32_t ___U3CvertexIndexU3E__1_6;
	// TMPro.Examples.VertexColorCycler TMPro.Examples.VertexColorCycler/<AnimateVertexColors>c__Iterator0::$this
	VertexColorCycler_t5EEC79CC79AA4B00DF4BE5CE390C12E47385299C * ___U24this_7;
	// System.Object TMPro.Examples.VertexColorCycler/<AnimateVertexColors>c__Iterator0::$current
	RuntimeObject * ___U24current_8;
	// System.Boolean TMPro.Examples.VertexColorCycler/<AnimateVertexColors>c__Iterator0::$disposing
	bool ___U24disposing_9;
	// System.Int32 TMPro.Examples.VertexColorCycler/<AnimateVertexColors>c__Iterator0::$PC
	int32_t ___U24PC_10;

public:
	inline static int32_t get_offset_of_U3CtextInfoU3E__0_0() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t7BBBF1D9BED81210183303D57EA4AEE43847D4BA, ___U3CtextInfoU3E__0_0)); }
	inline TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181 * get_U3CtextInfoU3E__0_0() const { return ___U3CtextInfoU3E__0_0; }
	inline TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181 ** get_address_of_U3CtextInfoU3E__0_0() { return &___U3CtextInfoU3E__0_0; }
	inline void set_U3CtextInfoU3E__0_0(TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181 * value)
	{
		___U3CtextInfoU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtextInfoU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CcurrentCharacterU3E__0_1() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t7BBBF1D9BED81210183303D57EA4AEE43847D4BA, ___U3CcurrentCharacterU3E__0_1)); }
	inline int32_t get_U3CcurrentCharacterU3E__0_1() const { return ___U3CcurrentCharacterU3E__0_1; }
	inline int32_t* get_address_of_U3CcurrentCharacterU3E__0_1() { return &___U3CcurrentCharacterU3E__0_1; }
	inline void set_U3CcurrentCharacterU3E__0_1(int32_t value)
	{
		___U3CcurrentCharacterU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3Cc0U3E__0_2() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t7BBBF1D9BED81210183303D57EA4AEE43847D4BA, ___U3Cc0U3E__0_2)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_U3Cc0U3E__0_2() const { return ___U3Cc0U3E__0_2; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_U3Cc0U3E__0_2() { return &___U3Cc0U3E__0_2; }
	inline void set_U3Cc0U3E__0_2(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___U3Cc0U3E__0_2 = value;
	}

	inline static int32_t get_offset_of_U3CcharacterCountU3E__1_3() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t7BBBF1D9BED81210183303D57EA4AEE43847D4BA, ___U3CcharacterCountU3E__1_3)); }
	inline int32_t get_U3CcharacterCountU3E__1_3() const { return ___U3CcharacterCountU3E__1_3; }
	inline int32_t* get_address_of_U3CcharacterCountU3E__1_3() { return &___U3CcharacterCountU3E__1_3; }
	inline void set_U3CcharacterCountU3E__1_3(int32_t value)
	{
		___U3CcharacterCountU3E__1_3 = value;
	}

	inline static int32_t get_offset_of_U3CmaterialIndexU3E__1_4() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t7BBBF1D9BED81210183303D57EA4AEE43847D4BA, ___U3CmaterialIndexU3E__1_4)); }
	inline int32_t get_U3CmaterialIndexU3E__1_4() const { return ___U3CmaterialIndexU3E__1_4; }
	inline int32_t* get_address_of_U3CmaterialIndexU3E__1_4() { return &___U3CmaterialIndexU3E__1_4; }
	inline void set_U3CmaterialIndexU3E__1_4(int32_t value)
	{
		___U3CmaterialIndexU3E__1_4 = value;
	}

	inline static int32_t get_offset_of_U3CnewVertexColorsU3E__1_5() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t7BBBF1D9BED81210183303D57EA4AEE43847D4BA, ___U3CnewVertexColorsU3E__1_5)); }
	inline Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983* get_U3CnewVertexColorsU3E__1_5() const { return ___U3CnewVertexColorsU3E__1_5; }
	inline Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983** get_address_of_U3CnewVertexColorsU3E__1_5() { return &___U3CnewVertexColorsU3E__1_5; }
	inline void set_U3CnewVertexColorsU3E__1_5(Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983* value)
	{
		___U3CnewVertexColorsU3E__1_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnewVertexColorsU3E__1_5), value);
	}

	inline static int32_t get_offset_of_U3CvertexIndexU3E__1_6() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t7BBBF1D9BED81210183303D57EA4AEE43847D4BA, ___U3CvertexIndexU3E__1_6)); }
	inline int32_t get_U3CvertexIndexU3E__1_6() const { return ___U3CvertexIndexU3E__1_6; }
	inline int32_t* get_address_of_U3CvertexIndexU3E__1_6() { return &___U3CvertexIndexU3E__1_6; }
	inline void set_U3CvertexIndexU3E__1_6(int32_t value)
	{
		___U3CvertexIndexU3E__1_6 = value;
	}

	inline static int32_t get_offset_of_U24this_7() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t7BBBF1D9BED81210183303D57EA4AEE43847D4BA, ___U24this_7)); }
	inline VertexColorCycler_t5EEC79CC79AA4B00DF4BE5CE390C12E47385299C * get_U24this_7() const { return ___U24this_7; }
	inline VertexColorCycler_t5EEC79CC79AA4B00DF4BE5CE390C12E47385299C ** get_address_of_U24this_7() { return &___U24this_7; }
	inline void set_U24this_7(VertexColorCycler_t5EEC79CC79AA4B00DF4BE5CE390C12E47385299C * value)
	{
		___U24this_7 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_7), value);
	}

	inline static int32_t get_offset_of_U24current_8() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t7BBBF1D9BED81210183303D57EA4AEE43847D4BA, ___U24current_8)); }
	inline RuntimeObject * get_U24current_8() const { return ___U24current_8; }
	inline RuntimeObject ** get_address_of_U24current_8() { return &___U24current_8; }
	inline void set_U24current_8(RuntimeObject * value)
	{
		___U24current_8 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_8), value);
	}

	inline static int32_t get_offset_of_U24disposing_9() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t7BBBF1D9BED81210183303D57EA4AEE43847D4BA, ___U24disposing_9)); }
	inline bool get_U24disposing_9() const { return ___U24disposing_9; }
	inline bool* get_address_of_U24disposing_9() { return &___U24disposing_9; }
	inline void set_U24disposing_9(bool value)
	{
		___U24disposing_9 = value;
	}

	inline static int32_t get_offset_of_U24PC_10() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t7BBBF1D9BED81210183303D57EA4AEE43847D4BA, ___U24PC_10)); }
	inline int32_t get_U24PC_10() const { return ___U24PC_10; }
	inline int32_t* get_address_of_U24PC_10() { return &___U24PC_10; }
	inline void set_U24PC_10(int32_t value)
	{
		___U24PC_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CANIMATEVERTEXCOLORSU3EC__ITERATOR0_T7BBBF1D9BED81210183303D57EA4AEE43847D4BA_H
#ifndef U3CANIMATEVERTEXCOLORSU3EC__ITERATOR0_TFA247E5BC602CE6265BA54F19C1C496C28DABE6D_H
#define U3CANIMATEVERTEXCOLORSU3EC__ITERATOR0_TFA247E5BC602CE6265BA54F19C1C496C28DABE6D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.VertexJitter/<AnimateVertexColors>c__Iterator0
struct  U3CAnimateVertexColorsU3Ec__Iterator0_tFA247E5BC602CE6265BA54F19C1C496C28DABE6D  : public RuntimeObject
{
public:
	// TMPro.TMP_TextInfo TMPro.Examples.VertexJitter/<AnimateVertexColors>c__Iterator0::<textInfo>__0
	TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181 * ___U3CtextInfoU3E__0_0;
	// System.Int32 TMPro.Examples.VertexJitter/<AnimateVertexColors>c__Iterator0::<loopCount>__0
	int32_t ___U3CloopCountU3E__0_1;
	// TMPro.Examples.VertexJitter/VertexAnim[] TMPro.Examples.VertexJitter/<AnimateVertexColors>c__Iterator0::<vertexAnim>__0
	VertexAnimU5BU5D_tFDF75428176C5B23AB66CAAECB9202F55DA6E13F* ___U3CvertexAnimU3E__0_2;
	// TMPro.TMP_MeshInfo[] TMPro.Examples.VertexJitter/<AnimateVertexColors>c__Iterator0::<cachedMeshInfo>__0
	TMP_MeshInfoU5BU5D_t7F7564862ADABD75DAD9B09FF274591F807FFDE9* ___U3CcachedMeshInfoU3E__0_3;
	// System.Int32 TMPro.Examples.VertexJitter/<AnimateVertexColors>c__Iterator0::<characterCount>__1
	int32_t ___U3CcharacterCountU3E__1_4;
	// UnityEngine.Matrix4x4 TMPro.Examples.VertexJitter/<AnimateVertexColors>c__Iterator0::<matrix>__2
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___U3CmatrixU3E__2_5;
	// TMPro.Examples.VertexJitter TMPro.Examples.VertexJitter/<AnimateVertexColors>c__Iterator0::$this
	VertexJitter_tF4F1AE4AF51C09E6FE57F91227A228B61BD0ACF0 * ___U24this_6;
	// System.Object TMPro.Examples.VertexJitter/<AnimateVertexColors>c__Iterator0::$current
	RuntimeObject * ___U24current_7;
	// System.Boolean TMPro.Examples.VertexJitter/<AnimateVertexColors>c__Iterator0::$disposing
	bool ___U24disposing_8;
	// System.Int32 TMPro.Examples.VertexJitter/<AnimateVertexColors>c__Iterator0::$PC
	int32_t ___U24PC_9;

public:
	inline static int32_t get_offset_of_U3CtextInfoU3E__0_0() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_tFA247E5BC602CE6265BA54F19C1C496C28DABE6D, ___U3CtextInfoU3E__0_0)); }
	inline TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181 * get_U3CtextInfoU3E__0_0() const { return ___U3CtextInfoU3E__0_0; }
	inline TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181 ** get_address_of_U3CtextInfoU3E__0_0() { return &___U3CtextInfoU3E__0_0; }
	inline void set_U3CtextInfoU3E__0_0(TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181 * value)
	{
		___U3CtextInfoU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtextInfoU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CloopCountU3E__0_1() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_tFA247E5BC602CE6265BA54F19C1C496C28DABE6D, ___U3CloopCountU3E__0_1)); }
	inline int32_t get_U3CloopCountU3E__0_1() const { return ___U3CloopCountU3E__0_1; }
	inline int32_t* get_address_of_U3CloopCountU3E__0_1() { return &___U3CloopCountU3E__0_1; }
	inline void set_U3CloopCountU3E__0_1(int32_t value)
	{
		___U3CloopCountU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3CvertexAnimU3E__0_2() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_tFA247E5BC602CE6265BA54F19C1C496C28DABE6D, ___U3CvertexAnimU3E__0_2)); }
	inline VertexAnimU5BU5D_tFDF75428176C5B23AB66CAAECB9202F55DA6E13F* get_U3CvertexAnimU3E__0_2() const { return ___U3CvertexAnimU3E__0_2; }
	inline VertexAnimU5BU5D_tFDF75428176C5B23AB66CAAECB9202F55DA6E13F** get_address_of_U3CvertexAnimU3E__0_2() { return &___U3CvertexAnimU3E__0_2; }
	inline void set_U3CvertexAnimU3E__0_2(VertexAnimU5BU5D_tFDF75428176C5B23AB66CAAECB9202F55DA6E13F* value)
	{
		___U3CvertexAnimU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CvertexAnimU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U3CcachedMeshInfoU3E__0_3() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_tFA247E5BC602CE6265BA54F19C1C496C28DABE6D, ___U3CcachedMeshInfoU3E__0_3)); }
	inline TMP_MeshInfoU5BU5D_t7F7564862ADABD75DAD9B09FF274591F807FFDE9* get_U3CcachedMeshInfoU3E__0_3() const { return ___U3CcachedMeshInfoU3E__0_3; }
	inline TMP_MeshInfoU5BU5D_t7F7564862ADABD75DAD9B09FF274591F807FFDE9** get_address_of_U3CcachedMeshInfoU3E__0_3() { return &___U3CcachedMeshInfoU3E__0_3; }
	inline void set_U3CcachedMeshInfoU3E__0_3(TMP_MeshInfoU5BU5D_t7F7564862ADABD75DAD9B09FF274591F807FFDE9* value)
	{
		___U3CcachedMeshInfoU3E__0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcachedMeshInfoU3E__0_3), value);
	}

	inline static int32_t get_offset_of_U3CcharacterCountU3E__1_4() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_tFA247E5BC602CE6265BA54F19C1C496C28DABE6D, ___U3CcharacterCountU3E__1_4)); }
	inline int32_t get_U3CcharacterCountU3E__1_4() const { return ___U3CcharacterCountU3E__1_4; }
	inline int32_t* get_address_of_U3CcharacterCountU3E__1_4() { return &___U3CcharacterCountU3E__1_4; }
	inline void set_U3CcharacterCountU3E__1_4(int32_t value)
	{
		___U3CcharacterCountU3E__1_4 = value;
	}

	inline static int32_t get_offset_of_U3CmatrixU3E__2_5() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_tFA247E5BC602CE6265BA54F19C1C496C28DABE6D, ___U3CmatrixU3E__2_5)); }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  get_U3CmatrixU3E__2_5() const { return ___U3CmatrixU3E__2_5; }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * get_address_of_U3CmatrixU3E__2_5() { return &___U3CmatrixU3E__2_5; }
	inline void set_U3CmatrixU3E__2_5(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  value)
	{
		___U3CmatrixU3E__2_5 = value;
	}

	inline static int32_t get_offset_of_U24this_6() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_tFA247E5BC602CE6265BA54F19C1C496C28DABE6D, ___U24this_6)); }
	inline VertexJitter_tF4F1AE4AF51C09E6FE57F91227A228B61BD0ACF0 * get_U24this_6() const { return ___U24this_6; }
	inline VertexJitter_tF4F1AE4AF51C09E6FE57F91227A228B61BD0ACF0 ** get_address_of_U24this_6() { return &___U24this_6; }
	inline void set_U24this_6(VertexJitter_tF4F1AE4AF51C09E6FE57F91227A228B61BD0ACF0 * value)
	{
		___U24this_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_6), value);
	}

	inline static int32_t get_offset_of_U24current_7() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_tFA247E5BC602CE6265BA54F19C1C496C28DABE6D, ___U24current_7)); }
	inline RuntimeObject * get_U24current_7() const { return ___U24current_7; }
	inline RuntimeObject ** get_address_of_U24current_7() { return &___U24current_7; }
	inline void set_U24current_7(RuntimeObject * value)
	{
		___U24current_7 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_7), value);
	}

	inline static int32_t get_offset_of_U24disposing_8() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_tFA247E5BC602CE6265BA54F19C1C496C28DABE6D, ___U24disposing_8)); }
	inline bool get_U24disposing_8() const { return ___U24disposing_8; }
	inline bool* get_address_of_U24disposing_8() { return &___U24disposing_8; }
	inline void set_U24disposing_8(bool value)
	{
		___U24disposing_8 = value;
	}

	inline static int32_t get_offset_of_U24PC_9() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_tFA247E5BC602CE6265BA54F19C1C496C28DABE6D, ___U24PC_9)); }
	inline int32_t get_U24PC_9() const { return ___U24PC_9; }
	inline int32_t* get_address_of_U24PC_9() { return &___U24PC_9; }
	inline void set_U24PC_9(int32_t value)
	{
		___U24PC_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CANIMATEVERTEXCOLORSU3EC__ITERATOR0_TFA247E5BC602CE6265BA54F19C1C496C28DABE6D_H
#ifndef U3CANIMATEVERTEXCOLORSU3EC__ITERATOR0_T14AC1A090C4142BE438C54B95703A6AF8243626E_H
#define U3CANIMATEVERTEXCOLORSU3EC__ITERATOR0_T14AC1A090C4142BE438C54B95703A6AF8243626E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.VertexShakeA/<AnimateVertexColors>c__Iterator0
struct  U3CAnimateVertexColorsU3Ec__Iterator0_t14AC1A090C4142BE438C54B95703A6AF8243626E  : public RuntimeObject
{
public:
	// TMPro.TMP_TextInfo TMPro.Examples.VertexShakeA/<AnimateVertexColors>c__Iterator0::<textInfo>__0
	TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181 * ___U3CtextInfoU3E__0_0;
	// UnityEngine.Vector3[][] TMPro.Examples.VertexShakeA/<AnimateVertexColors>c__Iterator0::<copyOfVertices>__0
	Vector3U5BU5DU5BU5D_tB83B3D7B4B4D7AD0A199DD45DF2963AB05A519AC* ___U3CcopyOfVerticesU3E__0_1;
	// System.Int32 TMPro.Examples.VertexShakeA/<AnimateVertexColors>c__Iterator0::<characterCount>__1
	int32_t ___U3CcharacterCountU3E__1_2;
	// System.Int32 TMPro.Examples.VertexShakeA/<AnimateVertexColors>c__Iterator0::<lineCount>__1
	int32_t ___U3ClineCountU3E__1_3;
	// UnityEngine.Matrix4x4 TMPro.Examples.VertexShakeA/<AnimateVertexColors>c__Iterator0::<matrix>__2
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___U3CmatrixU3E__2_4;
	// TMPro.Examples.VertexShakeA TMPro.Examples.VertexShakeA/<AnimateVertexColors>c__Iterator0::$this
	VertexShakeA_tCBD5B7397DD332038CE795E6B0A4D5B199739C27 * ___U24this_5;
	// System.Object TMPro.Examples.VertexShakeA/<AnimateVertexColors>c__Iterator0::$current
	RuntimeObject * ___U24current_6;
	// System.Boolean TMPro.Examples.VertexShakeA/<AnimateVertexColors>c__Iterator0::$disposing
	bool ___U24disposing_7;
	// System.Int32 TMPro.Examples.VertexShakeA/<AnimateVertexColors>c__Iterator0::$PC
	int32_t ___U24PC_8;

public:
	inline static int32_t get_offset_of_U3CtextInfoU3E__0_0() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t14AC1A090C4142BE438C54B95703A6AF8243626E, ___U3CtextInfoU3E__0_0)); }
	inline TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181 * get_U3CtextInfoU3E__0_0() const { return ___U3CtextInfoU3E__0_0; }
	inline TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181 ** get_address_of_U3CtextInfoU3E__0_0() { return &___U3CtextInfoU3E__0_0; }
	inline void set_U3CtextInfoU3E__0_0(TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181 * value)
	{
		___U3CtextInfoU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtextInfoU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CcopyOfVerticesU3E__0_1() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t14AC1A090C4142BE438C54B95703A6AF8243626E, ___U3CcopyOfVerticesU3E__0_1)); }
	inline Vector3U5BU5DU5BU5D_tB83B3D7B4B4D7AD0A199DD45DF2963AB05A519AC* get_U3CcopyOfVerticesU3E__0_1() const { return ___U3CcopyOfVerticesU3E__0_1; }
	inline Vector3U5BU5DU5BU5D_tB83B3D7B4B4D7AD0A199DD45DF2963AB05A519AC** get_address_of_U3CcopyOfVerticesU3E__0_1() { return &___U3CcopyOfVerticesU3E__0_1; }
	inline void set_U3CcopyOfVerticesU3E__0_1(Vector3U5BU5DU5BU5D_tB83B3D7B4B4D7AD0A199DD45DF2963AB05A519AC* value)
	{
		___U3CcopyOfVerticesU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcopyOfVerticesU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U3CcharacterCountU3E__1_2() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t14AC1A090C4142BE438C54B95703A6AF8243626E, ___U3CcharacterCountU3E__1_2)); }
	inline int32_t get_U3CcharacterCountU3E__1_2() const { return ___U3CcharacterCountU3E__1_2; }
	inline int32_t* get_address_of_U3CcharacterCountU3E__1_2() { return &___U3CcharacterCountU3E__1_2; }
	inline void set_U3CcharacterCountU3E__1_2(int32_t value)
	{
		___U3CcharacterCountU3E__1_2 = value;
	}

	inline static int32_t get_offset_of_U3ClineCountU3E__1_3() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t14AC1A090C4142BE438C54B95703A6AF8243626E, ___U3ClineCountU3E__1_3)); }
	inline int32_t get_U3ClineCountU3E__1_3() const { return ___U3ClineCountU3E__1_3; }
	inline int32_t* get_address_of_U3ClineCountU3E__1_3() { return &___U3ClineCountU3E__1_3; }
	inline void set_U3ClineCountU3E__1_3(int32_t value)
	{
		___U3ClineCountU3E__1_3 = value;
	}

	inline static int32_t get_offset_of_U3CmatrixU3E__2_4() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t14AC1A090C4142BE438C54B95703A6AF8243626E, ___U3CmatrixU3E__2_4)); }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  get_U3CmatrixU3E__2_4() const { return ___U3CmatrixU3E__2_4; }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * get_address_of_U3CmatrixU3E__2_4() { return &___U3CmatrixU3E__2_4; }
	inline void set_U3CmatrixU3E__2_4(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  value)
	{
		___U3CmatrixU3E__2_4 = value;
	}

	inline static int32_t get_offset_of_U24this_5() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t14AC1A090C4142BE438C54B95703A6AF8243626E, ___U24this_5)); }
	inline VertexShakeA_tCBD5B7397DD332038CE795E6B0A4D5B199739C27 * get_U24this_5() const { return ___U24this_5; }
	inline VertexShakeA_tCBD5B7397DD332038CE795E6B0A4D5B199739C27 ** get_address_of_U24this_5() { return &___U24this_5; }
	inline void set_U24this_5(VertexShakeA_tCBD5B7397DD332038CE795E6B0A4D5B199739C27 * value)
	{
		___U24this_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_5), value);
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t14AC1A090C4142BE438C54B95703A6AF8243626E, ___U24current_6)); }
	inline RuntimeObject * get_U24current_6() const { return ___U24current_6; }
	inline RuntimeObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(RuntimeObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_6), value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t14AC1A090C4142BE438C54B95703A6AF8243626E, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t14AC1A090C4142BE438C54B95703A6AF8243626E, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CANIMATEVERTEXCOLORSU3EC__ITERATOR0_T14AC1A090C4142BE438C54B95703A6AF8243626E_H
#ifndef U3CANIMATEVERTEXCOLORSU3EC__ITERATOR0_T7ECD09F2BEC8C31A570058712068497BDEAB23E4_H
#define U3CANIMATEVERTEXCOLORSU3EC__ITERATOR0_T7ECD09F2BEC8C31A570058712068497BDEAB23E4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.VertexShakeB/<AnimateVertexColors>c__Iterator0
struct  U3CAnimateVertexColorsU3Ec__Iterator0_t7ECD09F2BEC8C31A570058712068497BDEAB23E4  : public RuntimeObject
{
public:
	// TMPro.TMP_TextInfo TMPro.Examples.VertexShakeB/<AnimateVertexColors>c__Iterator0::<textInfo>__0
	TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181 * ___U3CtextInfoU3E__0_0;
	// UnityEngine.Vector3[][] TMPro.Examples.VertexShakeB/<AnimateVertexColors>c__Iterator0::<copyOfVertices>__0
	Vector3U5BU5DU5BU5D_tB83B3D7B4B4D7AD0A199DD45DF2963AB05A519AC* ___U3CcopyOfVerticesU3E__0_1;
	// System.Int32 TMPro.Examples.VertexShakeB/<AnimateVertexColors>c__Iterator0::<characterCount>__1
	int32_t ___U3CcharacterCountU3E__1_2;
	// System.Int32 TMPro.Examples.VertexShakeB/<AnimateVertexColors>c__Iterator0::<lineCount>__1
	int32_t ___U3ClineCountU3E__1_3;
	// UnityEngine.Matrix4x4 TMPro.Examples.VertexShakeB/<AnimateVertexColors>c__Iterator0::<matrix>__2
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___U3CmatrixU3E__2_4;
	// TMPro.Examples.VertexShakeB TMPro.Examples.VertexShakeB/<AnimateVertexColors>c__Iterator0::$this
	VertexShakeB_tC2BBE39E80035E065C9ADB888AA52006D10EDC0E * ___U24this_5;
	// System.Object TMPro.Examples.VertexShakeB/<AnimateVertexColors>c__Iterator0::$current
	RuntimeObject * ___U24current_6;
	// System.Boolean TMPro.Examples.VertexShakeB/<AnimateVertexColors>c__Iterator0::$disposing
	bool ___U24disposing_7;
	// System.Int32 TMPro.Examples.VertexShakeB/<AnimateVertexColors>c__Iterator0::$PC
	int32_t ___U24PC_8;

public:
	inline static int32_t get_offset_of_U3CtextInfoU3E__0_0() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t7ECD09F2BEC8C31A570058712068497BDEAB23E4, ___U3CtextInfoU3E__0_0)); }
	inline TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181 * get_U3CtextInfoU3E__0_0() const { return ___U3CtextInfoU3E__0_0; }
	inline TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181 ** get_address_of_U3CtextInfoU3E__0_0() { return &___U3CtextInfoU3E__0_0; }
	inline void set_U3CtextInfoU3E__0_0(TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181 * value)
	{
		___U3CtextInfoU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtextInfoU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CcopyOfVerticesU3E__0_1() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t7ECD09F2BEC8C31A570058712068497BDEAB23E4, ___U3CcopyOfVerticesU3E__0_1)); }
	inline Vector3U5BU5DU5BU5D_tB83B3D7B4B4D7AD0A199DD45DF2963AB05A519AC* get_U3CcopyOfVerticesU3E__0_1() const { return ___U3CcopyOfVerticesU3E__0_1; }
	inline Vector3U5BU5DU5BU5D_tB83B3D7B4B4D7AD0A199DD45DF2963AB05A519AC** get_address_of_U3CcopyOfVerticesU3E__0_1() { return &___U3CcopyOfVerticesU3E__0_1; }
	inline void set_U3CcopyOfVerticesU3E__0_1(Vector3U5BU5DU5BU5D_tB83B3D7B4B4D7AD0A199DD45DF2963AB05A519AC* value)
	{
		___U3CcopyOfVerticesU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcopyOfVerticesU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U3CcharacterCountU3E__1_2() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t7ECD09F2BEC8C31A570058712068497BDEAB23E4, ___U3CcharacterCountU3E__1_2)); }
	inline int32_t get_U3CcharacterCountU3E__1_2() const { return ___U3CcharacterCountU3E__1_2; }
	inline int32_t* get_address_of_U3CcharacterCountU3E__1_2() { return &___U3CcharacterCountU3E__1_2; }
	inline void set_U3CcharacterCountU3E__1_2(int32_t value)
	{
		___U3CcharacterCountU3E__1_2 = value;
	}

	inline static int32_t get_offset_of_U3ClineCountU3E__1_3() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t7ECD09F2BEC8C31A570058712068497BDEAB23E4, ___U3ClineCountU3E__1_3)); }
	inline int32_t get_U3ClineCountU3E__1_3() const { return ___U3ClineCountU3E__1_3; }
	inline int32_t* get_address_of_U3ClineCountU3E__1_3() { return &___U3ClineCountU3E__1_3; }
	inline void set_U3ClineCountU3E__1_3(int32_t value)
	{
		___U3ClineCountU3E__1_3 = value;
	}

	inline static int32_t get_offset_of_U3CmatrixU3E__2_4() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t7ECD09F2BEC8C31A570058712068497BDEAB23E4, ___U3CmatrixU3E__2_4)); }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  get_U3CmatrixU3E__2_4() const { return ___U3CmatrixU3E__2_4; }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * get_address_of_U3CmatrixU3E__2_4() { return &___U3CmatrixU3E__2_4; }
	inline void set_U3CmatrixU3E__2_4(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  value)
	{
		___U3CmatrixU3E__2_4 = value;
	}

	inline static int32_t get_offset_of_U24this_5() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t7ECD09F2BEC8C31A570058712068497BDEAB23E4, ___U24this_5)); }
	inline VertexShakeB_tC2BBE39E80035E065C9ADB888AA52006D10EDC0E * get_U24this_5() const { return ___U24this_5; }
	inline VertexShakeB_tC2BBE39E80035E065C9ADB888AA52006D10EDC0E ** get_address_of_U24this_5() { return &___U24this_5; }
	inline void set_U24this_5(VertexShakeB_tC2BBE39E80035E065C9ADB888AA52006D10EDC0E * value)
	{
		___U24this_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_5), value);
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t7ECD09F2BEC8C31A570058712068497BDEAB23E4, ___U24current_6)); }
	inline RuntimeObject * get_U24current_6() const { return ___U24current_6; }
	inline RuntimeObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(RuntimeObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_6), value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t7ECD09F2BEC8C31A570058712068497BDEAB23E4, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t7ECD09F2BEC8C31A570058712068497BDEAB23E4, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CANIMATEVERTEXCOLORSU3EC__ITERATOR0_T7ECD09F2BEC8C31A570058712068497BDEAB23E4_H
#ifndef U3CANIMATEVERTEXCOLORSU3EC__ITERATOR0_T4CE04A71641C727D7CFD708F629B27504B50CE7D_H
#define U3CANIMATEVERTEXCOLORSU3EC__ITERATOR0_T4CE04A71641C727D7CFD708F629B27504B50CE7D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.VertexZoom/<AnimateVertexColors>c__Iterator0
struct  U3CAnimateVertexColorsU3Ec__Iterator0_t4CE04A71641C727D7CFD708F629B27504B50CE7D  : public RuntimeObject
{
public:
	// TMPro.TMP_TextInfo TMPro.Examples.VertexZoom/<AnimateVertexColors>c__Iterator0::<textInfo>__0
	TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181 * ___U3CtextInfoU3E__0_0;
	// TMPro.TMP_MeshInfo[] TMPro.Examples.VertexZoom/<AnimateVertexColors>c__Iterator0::<cachedMeshInfoVertexData>__0
	TMP_MeshInfoU5BU5D_t7F7564862ADABD75DAD9B09FF274591F807FFDE9* ___U3CcachedMeshInfoVertexDataU3E__0_1;
	// System.Collections.Generic.List`1<System.Int32> TMPro.Examples.VertexZoom/<AnimateVertexColors>c__Iterator0::<scaleSortingOrder>__0
	List_1_t4A961510635950236678F1B3B2436ECCAF28713A * ___U3CscaleSortingOrderU3E__0_2;
	// System.Int32 TMPro.Examples.VertexZoom/<AnimateVertexColors>c__Iterator0::<characterCount>__1
	int32_t ___U3CcharacterCountU3E__1_3;
	// UnityEngine.Matrix4x4 TMPro.Examples.VertexZoom/<AnimateVertexColors>c__Iterator0::<matrix>__2
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___U3CmatrixU3E__2_4;
	// TMPro.Examples.VertexZoom TMPro.Examples.VertexZoom/<AnimateVertexColors>c__Iterator0::$this
	VertexZoom_t78EE9E8C38B64E198806ADD562E22F722A2EE156 * ___U24this_5;
	// System.Object TMPro.Examples.VertexZoom/<AnimateVertexColors>c__Iterator0::$current
	RuntimeObject * ___U24current_6;
	// System.Boolean TMPro.Examples.VertexZoom/<AnimateVertexColors>c__Iterator0::$disposing
	bool ___U24disposing_7;
	// System.Int32 TMPro.Examples.VertexZoom/<AnimateVertexColors>c__Iterator0::$PC
	int32_t ___U24PC_8;
	// TMPro.Examples.VertexZoom/<AnimateVertexColors>c__Iterator0/<AnimateVertexColors>c__AnonStorey1 TMPro.Examples.VertexZoom/<AnimateVertexColors>c__Iterator0::$locvar0
	U3CAnimateVertexColorsU3Ec__AnonStorey1_t22561508C8B53D4F45837DFC591B58212EEB80D6 * ___U24locvar0_9;

public:
	inline static int32_t get_offset_of_U3CtextInfoU3E__0_0() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t4CE04A71641C727D7CFD708F629B27504B50CE7D, ___U3CtextInfoU3E__0_0)); }
	inline TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181 * get_U3CtextInfoU3E__0_0() const { return ___U3CtextInfoU3E__0_0; }
	inline TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181 ** get_address_of_U3CtextInfoU3E__0_0() { return &___U3CtextInfoU3E__0_0; }
	inline void set_U3CtextInfoU3E__0_0(TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181 * value)
	{
		___U3CtextInfoU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtextInfoU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CcachedMeshInfoVertexDataU3E__0_1() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t4CE04A71641C727D7CFD708F629B27504B50CE7D, ___U3CcachedMeshInfoVertexDataU3E__0_1)); }
	inline TMP_MeshInfoU5BU5D_t7F7564862ADABD75DAD9B09FF274591F807FFDE9* get_U3CcachedMeshInfoVertexDataU3E__0_1() const { return ___U3CcachedMeshInfoVertexDataU3E__0_1; }
	inline TMP_MeshInfoU5BU5D_t7F7564862ADABD75DAD9B09FF274591F807FFDE9** get_address_of_U3CcachedMeshInfoVertexDataU3E__0_1() { return &___U3CcachedMeshInfoVertexDataU3E__0_1; }
	inline void set_U3CcachedMeshInfoVertexDataU3E__0_1(TMP_MeshInfoU5BU5D_t7F7564862ADABD75DAD9B09FF274591F807FFDE9* value)
	{
		___U3CcachedMeshInfoVertexDataU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcachedMeshInfoVertexDataU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U3CscaleSortingOrderU3E__0_2() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t4CE04A71641C727D7CFD708F629B27504B50CE7D, ___U3CscaleSortingOrderU3E__0_2)); }
	inline List_1_t4A961510635950236678F1B3B2436ECCAF28713A * get_U3CscaleSortingOrderU3E__0_2() const { return ___U3CscaleSortingOrderU3E__0_2; }
	inline List_1_t4A961510635950236678F1B3B2436ECCAF28713A ** get_address_of_U3CscaleSortingOrderU3E__0_2() { return &___U3CscaleSortingOrderU3E__0_2; }
	inline void set_U3CscaleSortingOrderU3E__0_2(List_1_t4A961510635950236678F1B3B2436ECCAF28713A * value)
	{
		___U3CscaleSortingOrderU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CscaleSortingOrderU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U3CcharacterCountU3E__1_3() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t4CE04A71641C727D7CFD708F629B27504B50CE7D, ___U3CcharacterCountU3E__1_3)); }
	inline int32_t get_U3CcharacterCountU3E__1_3() const { return ___U3CcharacterCountU3E__1_3; }
	inline int32_t* get_address_of_U3CcharacterCountU3E__1_3() { return &___U3CcharacterCountU3E__1_3; }
	inline void set_U3CcharacterCountU3E__1_3(int32_t value)
	{
		___U3CcharacterCountU3E__1_3 = value;
	}

	inline static int32_t get_offset_of_U3CmatrixU3E__2_4() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t4CE04A71641C727D7CFD708F629B27504B50CE7D, ___U3CmatrixU3E__2_4)); }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  get_U3CmatrixU3E__2_4() const { return ___U3CmatrixU3E__2_4; }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * get_address_of_U3CmatrixU3E__2_4() { return &___U3CmatrixU3E__2_4; }
	inline void set_U3CmatrixU3E__2_4(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  value)
	{
		___U3CmatrixU3E__2_4 = value;
	}

	inline static int32_t get_offset_of_U24this_5() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t4CE04A71641C727D7CFD708F629B27504B50CE7D, ___U24this_5)); }
	inline VertexZoom_t78EE9E8C38B64E198806ADD562E22F722A2EE156 * get_U24this_5() const { return ___U24this_5; }
	inline VertexZoom_t78EE9E8C38B64E198806ADD562E22F722A2EE156 ** get_address_of_U24this_5() { return &___U24this_5; }
	inline void set_U24this_5(VertexZoom_t78EE9E8C38B64E198806ADD562E22F722A2EE156 * value)
	{
		___U24this_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_5), value);
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t4CE04A71641C727D7CFD708F629B27504B50CE7D, ___U24current_6)); }
	inline RuntimeObject * get_U24current_6() const { return ___U24current_6; }
	inline RuntimeObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(RuntimeObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_6), value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t4CE04A71641C727D7CFD708F629B27504B50CE7D, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t4CE04A71641C727D7CFD708F629B27504B50CE7D, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}

	inline static int32_t get_offset_of_U24locvar0_9() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t4CE04A71641C727D7CFD708F629B27504B50CE7D, ___U24locvar0_9)); }
	inline U3CAnimateVertexColorsU3Ec__AnonStorey1_t22561508C8B53D4F45837DFC591B58212EEB80D6 * get_U24locvar0_9() const { return ___U24locvar0_9; }
	inline U3CAnimateVertexColorsU3Ec__AnonStorey1_t22561508C8B53D4F45837DFC591B58212EEB80D6 ** get_address_of_U24locvar0_9() { return &___U24locvar0_9; }
	inline void set_U24locvar0_9(U3CAnimateVertexColorsU3Ec__AnonStorey1_t22561508C8B53D4F45837DFC591B58212EEB80D6 * value)
	{
		___U24locvar0_9 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar0_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CANIMATEVERTEXCOLORSU3EC__ITERATOR0_T4CE04A71641C727D7CFD708F629B27504B50CE7D_H
#ifndef U3CWARPTEXTU3EC__ITERATOR0_TB564D42ED024F833E335B6B0EB07DCC2F7BC438A_H
#define U3CWARPTEXTU3EC__ITERATOR0_TB564D42ED024F833E335B6B0EB07DCC2F7BC438A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.WarpTextExample/<WarpText>c__Iterator0
struct  U3CWarpTextU3Ec__Iterator0_tB564D42ED024F833E335B6B0EB07DCC2F7BC438A  : public RuntimeObject
{
public:
	// System.Single TMPro.Examples.WarpTextExample/<WarpText>c__Iterator0::<old_CurveScale>__0
	float ___U3Cold_CurveScaleU3E__0_0;
	// UnityEngine.AnimationCurve TMPro.Examples.WarpTextExample/<WarpText>c__Iterator0::<old_curve>__0
	AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * ___U3Cold_curveU3E__0_1;
	// TMPro.TMP_TextInfo TMPro.Examples.WarpTextExample/<WarpText>c__Iterator0::<textInfo>__1
	TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181 * ___U3CtextInfoU3E__1_2;
	// System.Int32 TMPro.Examples.WarpTextExample/<WarpText>c__Iterator0::<characterCount>__1
	int32_t ___U3CcharacterCountU3E__1_3;
	// System.Single TMPro.Examples.WarpTextExample/<WarpText>c__Iterator0::<boundsMinX>__1
	float ___U3CboundsMinXU3E__1_4;
	// System.Single TMPro.Examples.WarpTextExample/<WarpText>c__Iterator0::<boundsMaxX>__1
	float ___U3CboundsMaxXU3E__1_5;
	// UnityEngine.Vector3[] TMPro.Examples.WarpTextExample/<WarpText>c__Iterator0::<vertices>__2
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___U3CverticesU3E__2_6;
	// UnityEngine.Matrix4x4 TMPro.Examples.WarpTextExample/<WarpText>c__Iterator0::<matrix>__2
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___U3CmatrixU3E__2_7;
	// TMPro.Examples.WarpTextExample TMPro.Examples.WarpTextExample/<WarpText>c__Iterator0::$this
	WarpTextExample_t918FAC4E8BDAF99FAE70218E452EB4EF81A1C9F2 * ___U24this_8;
	// System.Object TMPro.Examples.WarpTextExample/<WarpText>c__Iterator0::$current
	RuntimeObject * ___U24current_9;
	// System.Boolean TMPro.Examples.WarpTextExample/<WarpText>c__Iterator0::$disposing
	bool ___U24disposing_10;
	// System.Int32 TMPro.Examples.WarpTextExample/<WarpText>c__Iterator0::$PC
	int32_t ___U24PC_11;

public:
	inline static int32_t get_offset_of_U3Cold_CurveScaleU3E__0_0() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_tB564D42ED024F833E335B6B0EB07DCC2F7BC438A, ___U3Cold_CurveScaleU3E__0_0)); }
	inline float get_U3Cold_CurveScaleU3E__0_0() const { return ___U3Cold_CurveScaleU3E__0_0; }
	inline float* get_address_of_U3Cold_CurveScaleU3E__0_0() { return &___U3Cold_CurveScaleU3E__0_0; }
	inline void set_U3Cold_CurveScaleU3E__0_0(float value)
	{
		___U3Cold_CurveScaleU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3Cold_curveU3E__0_1() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_tB564D42ED024F833E335B6B0EB07DCC2F7BC438A, ___U3Cold_curveU3E__0_1)); }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * get_U3Cold_curveU3E__0_1() const { return ___U3Cold_curveU3E__0_1; }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C ** get_address_of_U3Cold_curveU3E__0_1() { return &___U3Cold_curveU3E__0_1; }
	inline void set_U3Cold_curveU3E__0_1(AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * value)
	{
		___U3Cold_curveU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cold_curveU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U3CtextInfoU3E__1_2() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_tB564D42ED024F833E335B6B0EB07DCC2F7BC438A, ___U3CtextInfoU3E__1_2)); }
	inline TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181 * get_U3CtextInfoU3E__1_2() const { return ___U3CtextInfoU3E__1_2; }
	inline TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181 ** get_address_of_U3CtextInfoU3E__1_2() { return &___U3CtextInfoU3E__1_2; }
	inline void set_U3CtextInfoU3E__1_2(TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181 * value)
	{
		___U3CtextInfoU3E__1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtextInfoU3E__1_2), value);
	}

	inline static int32_t get_offset_of_U3CcharacterCountU3E__1_3() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_tB564D42ED024F833E335B6B0EB07DCC2F7BC438A, ___U3CcharacterCountU3E__1_3)); }
	inline int32_t get_U3CcharacterCountU3E__1_3() const { return ___U3CcharacterCountU3E__1_3; }
	inline int32_t* get_address_of_U3CcharacterCountU3E__1_3() { return &___U3CcharacterCountU3E__1_3; }
	inline void set_U3CcharacterCountU3E__1_3(int32_t value)
	{
		___U3CcharacterCountU3E__1_3 = value;
	}

	inline static int32_t get_offset_of_U3CboundsMinXU3E__1_4() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_tB564D42ED024F833E335B6B0EB07DCC2F7BC438A, ___U3CboundsMinXU3E__1_4)); }
	inline float get_U3CboundsMinXU3E__1_4() const { return ___U3CboundsMinXU3E__1_4; }
	inline float* get_address_of_U3CboundsMinXU3E__1_4() { return &___U3CboundsMinXU3E__1_4; }
	inline void set_U3CboundsMinXU3E__1_4(float value)
	{
		___U3CboundsMinXU3E__1_4 = value;
	}

	inline static int32_t get_offset_of_U3CboundsMaxXU3E__1_5() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_tB564D42ED024F833E335B6B0EB07DCC2F7BC438A, ___U3CboundsMaxXU3E__1_5)); }
	inline float get_U3CboundsMaxXU3E__1_5() const { return ___U3CboundsMaxXU3E__1_5; }
	inline float* get_address_of_U3CboundsMaxXU3E__1_5() { return &___U3CboundsMaxXU3E__1_5; }
	inline void set_U3CboundsMaxXU3E__1_5(float value)
	{
		___U3CboundsMaxXU3E__1_5 = value;
	}

	inline static int32_t get_offset_of_U3CverticesU3E__2_6() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_tB564D42ED024F833E335B6B0EB07DCC2F7BC438A, ___U3CverticesU3E__2_6)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_U3CverticesU3E__2_6() const { return ___U3CverticesU3E__2_6; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_U3CverticesU3E__2_6() { return &___U3CverticesU3E__2_6; }
	inline void set_U3CverticesU3E__2_6(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___U3CverticesU3E__2_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CverticesU3E__2_6), value);
	}

	inline static int32_t get_offset_of_U3CmatrixU3E__2_7() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_tB564D42ED024F833E335B6B0EB07DCC2F7BC438A, ___U3CmatrixU3E__2_7)); }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  get_U3CmatrixU3E__2_7() const { return ___U3CmatrixU3E__2_7; }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * get_address_of_U3CmatrixU3E__2_7() { return &___U3CmatrixU3E__2_7; }
	inline void set_U3CmatrixU3E__2_7(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  value)
	{
		___U3CmatrixU3E__2_7 = value;
	}

	inline static int32_t get_offset_of_U24this_8() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_tB564D42ED024F833E335B6B0EB07DCC2F7BC438A, ___U24this_8)); }
	inline WarpTextExample_t918FAC4E8BDAF99FAE70218E452EB4EF81A1C9F2 * get_U24this_8() const { return ___U24this_8; }
	inline WarpTextExample_t918FAC4E8BDAF99FAE70218E452EB4EF81A1C9F2 ** get_address_of_U24this_8() { return &___U24this_8; }
	inline void set_U24this_8(WarpTextExample_t918FAC4E8BDAF99FAE70218E452EB4EF81A1C9F2 * value)
	{
		___U24this_8 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_8), value);
	}

	inline static int32_t get_offset_of_U24current_9() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_tB564D42ED024F833E335B6B0EB07DCC2F7BC438A, ___U24current_9)); }
	inline RuntimeObject * get_U24current_9() const { return ___U24current_9; }
	inline RuntimeObject ** get_address_of_U24current_9() { return &___U24current_9; }
	inline void set_U24current_9(RuntimeObject * value)
	{
		___U24current_9 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_9), value);
	}

	inline static int32_t get_offset_of_U24disposing_10() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_tB564D42ED024F833E335B6B0EB07DCC2F7BC438A, ___U24disposing_10)); }
	inline bool get_U24disposing_10() const { return ___U24disposing_10; }
	inline bool* get_address_of_U24disposing_10() { return &___U24disposing_10; }
	inline void set_U24disposing_10(bool value)
	{
		___U24disposing_10 = value;
	}

	inline static int32_t get_offset_of_U24PC_11() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_tB564D42ED024F833E335B6B0EB07DCC2F7BC438A, ___U24PC_11)); }
	inline int32_t get_U24PC_11() const { return ___U24PC_11; }
	inline int32_t* get_address_of_U24PC_11() { return &___U24PC_11; }
	inline void set_U24PC_11(int32_t value)
	{
		___U24PC_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CWARPTEXTU3EC__ITERATOR0_TB564D42ED024F833E335B6B0EB07DCC2F7BC438A_H
#ifndef CHARACTERSELECTIONEVENT_T346DE6835B0DF86A32928016EE2C413E919DBF90_H
#define CHARACTERSELECTIONEVENT_T346DE6835B0DF86A32928016EE2C413E919DBF90_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_TextEventHandler/CharacterSelectionEvent
struct  CharacterSelectionEvent_t346DE6835B0DF86A32928016EE2C413E919DBF90  : public UnityEvent_2_t847360AB318A8D3485A7F2D074D4488D2132FA93
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHARACTERSELECTIONEVENT_T346DE6835B0DF86A32928016EE2C413E919DBF90_H
#ifndef LINESELECTIONEVENT_T9F223C373E9EF88E12570D86DE2651F8EDEEAB3F_H
#define LINESELECTIONEVENT_T9F223C373E9EF88E12570D86DE2651F8EDEEAB3F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_TextEventHandler/LineSelectionEvent
struct  LineSelectionEvent_t9F223C373E9EF88E12570D86DE2651F8EDEEAB3F  : public UnityEvent_3_tF6326B92F2348C19417549314692990FA2279EAA
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINESELECTIONEVENT_T9F223C373E9EF88E12570D86DE2651F8EDEEAB3F_H
#ifndef LINKSELECTIONEVENT_T61E2583194386CC362B13606ECE2F840876A24E8_H
#define LINKSELECTIONEVENT_T61E2583194386CC362B13606ECE2F840876A24E8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_TextEventHandler/LinkSelectionEvent
struct  LinkSelectionEvent_t61E2583194386CC362B13606ECE2F840876A24E8  : public UnityEvent_3_tEAC12FDE9BD5A3DEBA11C8302B47F0BD504043A9
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINKSELECTIONEVENT_T61E2583194386CC362B13606ECE2F840876A24E8_H
#ifndef SPRITESELECTIONEVENT_TCE8FEB1D487ED84CBA38BC47F8949C5537672B32_H
#define SPRITESELECTIONEVENT_TCE8FEB1D487ED84CBA38BC47F8949C5537672B32_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_TextEventHandler/SpriteSelectionEvent
struct  SpriteSelectionEvent_tCE8FEB1D487ED84CBA38BC47F8949C5537672B32  : public UnityEvent_2_t847360AB318A8D3485A7F2D074D4488D2132FA93
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITESELECTIONEVENT_TCE8FEB1D487ED84CBA38BC47F8949C5537672B32_H
#ifndef WORDSELECTIONEVENT_TF37108E717AF8FCEB63C4B4CB11231A5F030A554_H
#define WORDSELECTIONEVENT_TF37108E717AF8FCEB63C4B4CB11231A5F030A554_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_TextEventHandler/WordSelectionEvent
struct  WordSelectionEvent_tF37108E717AF8FCEB63C4B4CB11231A5F030A554  : public UnityEvent_3_tF6326B92F2348C19417549314692990FA2279EAA
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WORDSELECTIONEVENT_TF37108E717AF8FCEB63C4B4CB11231A5F030A554_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#define SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734_marshaled_pinvoke : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734_marshaled_com : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#ifndef TMP_INPUTVALIDATOR_T4C673E12211AFB82AAF94D9DEA556FDC306E69CD_H
#define TMP_INPUTVALIDATOR_T4C673E12211AFB82AAF94D9DEA556FDC306E69CD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_InputValidator
struct  TMP_InputValidator_t4C673E12211AFB82AAF94D9DEA556FDC306E69CD  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_INPUTVALIDATOR_T4C673E12211AFB82AAF94D9DEA556FDC306E69CD_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef TMP_DIGITVALIDATOR_TD53B3EF123D04F923055895ED56555317D239AB5_H
#define TMP_DIGITVALIDATOR_TD53B3EF123D04F923055895ED56555317D239AB5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_DigitValidator
struct  TMP_DigitValidator_tD53B3EF123D04F923055895ED56555317D239AB5  : public TMP_InputValidator_t4C673E12211AFB82AAF94D9DEA556FDC306E69CD
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_DIGITVALIDATOR_TD53B3EF123D04F923055895ED56555317D239AB5_H
#ifndef TMP_PHONENUMBERVALIDATOR_T7EB41CFDB7C6AA586BF5AF04151FC2228F565BD2_H
#define TMP_PHONENUMBERVALIDATOR_T7EB41CFDB7C6AA586BF5AF04151FC2228F565BD2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_PhoneNumberValidator
struct  TMP_PhoneNumberValidator_t7EB41CFDB7C6AA586BF5AF04151FC2228F565BD2  : public TMP_InputValidator_t4C673E12211AFB82AAF94D9DEA556FDC306E69CD
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_PHONENUMBERVALIDATOR_T7EB41CFDB7C6AA586BF5AF04151FC2228F565BD2_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef TMP_EXAMPLESCRIPT_01_T4208AD096DD4FCE676E1EE1FA3F4DFD6341D81A8_H
#define TMP_EXAMPLESCRIPT_01_T4208AD096DD4FCE676E1EE1FA3F4DFD6341D81A8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TMP_ExampleScript_01
struct  TMP_ExampleScript_01_t4208AD096DD4FCE676E1EE1FA3F4DFD6341D81A8  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// TMPro.Examples.TMP_ExampleScript_01/objectType TMPro.Examples.TMP_ExampleScript_01::ObjectType
	int32_t ___ObjectType_4;
	// System.Boolean TMPro.Examples.TMP_ExampleScript_01::isStatic
	bool ___isStatic_5;
	// TMPro.TMP_Text TMPro.Examples.TMP_ExampleScript_01::m_text
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * ___m_text_6;
	// System.Int32 TMPro.Examples.TMP_ExampleScript_01::count
	int32_t ___count_8;

public:
	inline static int32_t get_offset_of_ObjectType_4() { return static_cast<int32_t>(offsetof(TMP_ExampleScript_01_t4208AD096DD4FCE676E1EE1FA3F4DFD6341D81A8, ___ObjectType_4)); }
	inline int32_t get_ObjectType_4() const { return ___ObjectType_4; }
	inline int32_t* get_address_of_ObjectType_4() { return &___ObjectType_4; }
	inline void set_ObjectType_4(int32_t value)
	{
		___ObjectType_4 = value;
	}

	inline static int32_t get_offset_of_isStatic_5() { return static_cast<int32_t>(offsetof(TMP_ExampleScript_01_t4208AD096DD4FCE676E1EE1FA3F4DFD6341D81A8, ___isStatic_5)); }
	inline bool get_isStatic_5() const { return ___isStatic_5; }
	inline bool* get_address_of_isStatic_5() { return &___isStatic_5; }
	inline void set_isStatic_5(bool value)
	{
		___isStatic_5 = value;
	}

	inline static int32_t get_offset_of_m_text_6() { return static_cast<int32_t>(offsetof(TMP_ExampleScript_01_t4208AD096DD4FCE676E1EE1FA3F4DFD6341D81A8, ___m_text_6)); }
	inline TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * get_m_text_6() const { return ___m_text_6; }
	inline TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 ** get_address_of_m_text_6() { return &___m_text_6; }
	inline void set_m_text_6(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * value)
	{
		___m_text_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_text_6), value);
	}

	inline static int32_t get_offset_of_count_8() { return static_cast<int32_t>(offsetof(TMP_ExampleScript_01_t4208AD096DD4FCE676E1EE1FA3F4DFD6341D81A8, ___count_8)); }
	inline int32_t get_count_8() const { return ___count_8; }
	inline int32_t* get_address_of_count_8() { return &___count_8; }
	inline void set_count_8(int32_t value)
	{
		___count_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_EXAMPLESCRIPT_01_T4208AD096DD4FCE676E1EE1FA3F4DFD6341D81A8_H
#ifndef TMP_FRAMERATECOUNTER_T154205AC6610245B31CEF1C182FA4B4862D9D07F_H
#define TMP_FRAMERATECOUNTER_T154205AC6610245B31CEF1C182FA4B4862D9D07F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TMP_FrameRateCounter
struct  TMP_FrameRateCounter_t154205AC6610245B31CEF1C182FA4B4862D9D07F  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single TMPro.Examples.TMP_FrameRateCounter::UpdateInterval
	float ___UpdateInterval_4;
	// System.Single TMPro.Examples.TMP_FrameRateCounter::m_LastInterval
	float ___m_LastInterval_5;
	// System.Int32 TMPro.Examples.TMP_FrameRateCounter::m_Frames
	int32_t ___m_Frames_6;
	// TMPro.Examples.TMP_FrameRateCounter/FpsCounterAnchorPositions TMPro.Examples.TMP_FrameRateCounter::AnchorPosition
	int32_t ___AnchorPosition_7;
	// System.String TMPro.Examples.TMP_FrameRateCounter::htmlColorTag
	String_t* ___htmlColorTag_8;
	// TMPro.TextMeshPro TMPro.Examples.TMP_FrameRateCounter::m_TextMeshPro
	TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2 * ___m_TextMeshPro_10;
	// UnityEngine.Transform TMPro.Examples.TMP_FrameRateCounter::m_frameCounter_transform
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___m_frameCounter_transform_11;
	// UnityEngine.Camera TMPro.Examples.TMP_FrameRateCounter::m_camera
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___m_camera_12;
	// TMPro.Examples.TMP_FrameRateCounter/FpsCounterAnchorPositions TMPro.Examples.TMP_FrameRateCounter::last_AnchorPosition
	int32_t ___last_AnchorPosition_13;

public:
	inline static int32_t get_offset_of_UpdateInterval_4() { return static_cast<int32_t>(offsetof(TMP_FrameRateCounter_t154205AC6610245B31CEF1C182FA4B4862D9D07F, ___UpdateInterval_4)); }
	inline float get_UpdateInterval_4() const { return ___UpdateInterval_4; }
	inline float* get_address_of_UpdateInterval_4() { return &___UpdateInterval_4; }
	inline void set_UpdateInterval_4(float value)
	{
		___UpdateInterval_4 = value;
	}

	inline static int32_t get_offset_of_m_LastInterval_5() { return static_cast<int32_t>(offsetof(TMP_FrameRateCounter_t154205AC6610245B31CEF1C182FA4B4862D9D07F, ___m_LastInterval_5)); }
	inline float get_m_LastInterval_5() const { return ___m_LastInterval_5; }
	inline float* get_address_of_m_LastInterval_5() { return &___m_LastInterval_5; }
	inline void set_m_LastInterval_5(float value)
	{
		___m_LastInterval_5 = value;
	}

	inline static int32_t get_offset_of_m_Frames_6() { return static_cast<int32_t>(offsetof(TMP_FrameRateCounter_t154205AC6610245B31CEF1C182FA4B4862D9D07F, ___m_Frames_6)); }
	inline int32_t get_m_Frames_6() const { return ___m_Frames_6; }
	inline int32_t* get_address_of_m_Frames_6() { return &___m_Frames_6; }
	inline void set_m_Frames_6(int32_t value)
	{
		___m_Frames_6 = value;
	}

	inline static int32_t get_offset_of_AnchorPosition_7() { return static_cast<int32_t>(offsetof(TMP_FrameRateCounter_t154205AC6610245B31CEF1C182FA4B4862D9D07F, ___AnchorPosition_7)); }
	inline int32_t get_AnchorPosition_7() const { return ___AnchorPosition_7; }
	inline int32_t* get_address_of_AnchorPosition_7() { return &___AnchorPosition_7; }
	inline void set_AnchorPosition_7(int32_t value)
	{
		___AnchorPosition_7 = value;
	}

	inline static int32_t get_offset_of_htmlColorTag_8() { return static_cast<int32_t>(offsetof(TMP_FrameRateCounter_t154205AC6610245B31CEF1C182FA4B4862D9D07F, ___htmlColorTag_8)); }
	inline String_t* get_htmlColorTag_8() const { return ___htmlColorTag_8; }
	inline String_t** get_address_of_htmlColorTag_8() { return &___htmlColorTag_8; }
	inline void set_htmlColorTag_8(String_t* value)
	{
		___htmlColorTag_8 = value;
		Il2CppCodeGenWriteBarrier((&___htmlColorTag_8), value);
	}

	inline static int32_t get_offset_of_m_TextMeshPro_10() { return static_cast<int32_t>(offsetof(TMP_FrameRateCounter_t154205AC6610245B31CEF1C182FA4B4862D9D07F, ___m_TextMeshPro_10)); }
	inline TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2 * get_m_TextMeshPro_10() const { return ___m_TextMeshPro_10; }
	inline TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2 ** get_address_of_m_TextMeshPro_10() { return &___m_TextMeshPro_10; }
	inline void set_m_TextMeshPro_10(TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2 * value)
	{
		___m_TextMeshPro_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextMeshPro_10), value);
	}

	inline static int32_t get_offset_of_m_frameCounter_transform_11() { return static_cast<int32_t>(offsetof(TMP_FrameRateCounter_t154205AC6610245B31CEF1C182FA4B4862D9D07F, ___m_frameCounter_transform_11)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_m_frameCounter_transform_11() const { return ___m_frameCounter_transform_11; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_m_frameCounter_transform_11() { return &___m_frameCounter_transform_11; }
	inline void set_m_frameCounter_transform_11(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___m_frameCounter_transform_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_frameCounter_transform_11), value);
	}

	inline static int32_t get_offset_of_m_camera_12() { return static_cast<int32_t>(offsetof(TMP_FrameRateCounter_t154205AC6610245B31CEF1C182FA4B4862D9D07F, ___m_camera_12)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get_m_camera_12() const { return ___m_camera_12; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of_m_camera_12() { return &___m_camera_12; }
	inline void set_m_camera_12(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		___m_camera_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_camera_12), value);
	}

	inline static int32_t get_offset_of_last_AnchorPosition_13() { return static_cast<int32_t>(offsetof(TMP_FrameRateCounter_t154205AC6610245B31CEF1C182FA4B4862D9D07F, ___last_AnchorPosition_13)); }
	inline int32_t get_last_AnchorPosition_13() const { return ___last_AnchorPosition_13; }
	inline int32_t* get_address_of_last_AnchorPosition_13() { return &___last_AnchorPosition_13; }
	inline void set_last_AnchorPosition_13(int32_t value)
	{
		___last_AnchorPosition_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_FRAMERATECOUNTER_T154205AC6610245B31CEF1C182FA4B4862D9D07F_H
#ifndef TMP_TEXTEVENTCHECK_T5AF390C2FFBE0FBCFA4F905503A504F5DD891CAB_H
#define TMP_TEXTEVENTCHECK_T5AF390C2FFBE0FBCFA4F905503A504F5DD891CAB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TMP_TextEventCheck
struct  TMP_TextEventCheck_t5AF390C2FFBE0FBCFA4F905503A504F5DD891CAB  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// TMPro.TMP_TextEventHandler TMPro.Examples.TMP_TextEventCheck::TextEventHandler
	TMP_TextEventHandler_t6046295E0F06C5138916541DBE6B476E6FE66B54 * ___TextEventHandler_4;

public:
	inline static int32_t get_offset_of_TextEventHandler_4() { return static_cast<int32_t>(offsetof(TMP_TextEventCheck_t5AF390C2FFBE0FBCFA4F905503A504F5DD891CAB, ___TextEventHandler_4)); }
	inline TMP_TextEventHandler_t6046295E0F06C5138916541DBE6B476E6FE66B54 * get_TextEventHandler_4() const { return ___TextEventHandler_4; }
	inline TMP_TextEventHandler_t6046295E0F06C5138916541DBE6B476E6FE66B54 ** get_address_of_TextEventHandler_4() { return &___TextEventHandler_4; }
	inline void set_TextEventHandler_4(TMP_TextEventHandler_t6046295E0F06C5138916541DBE6B476E6FE66B54 * value)
	{
		___TextEventHandler_4 = value;
		Il2CppCodeGenWriteBarrier((&___TextEventHandler_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_TEXTEVENTCHECK_T5AF390C2FFBE0FBCFA4F905503A504F5DD891CAB_H
#ifndef TMP_TEXTINFODEBUGTOOL_T327D46DA1CC9A18738A134F109EA5AD3F8437FE7_H
#define TMP_TEXTINFODEBUGTOOL_T327D46DA1CC9A18738A134F109EA5AD3F8437FE7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TMP_TextInfoDebugTool
struct  TMP_TextInfoDebugTool_t327D46DA1CC9A18738A134F109EA5AD3F8437FE7  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean TMPro.Examples.TMP_TextInfoDebugTool::ShowCharacters
	bool ___ShowCharacters_4;
	// System.Boolean TMPro.Examples.TMP_TextInfoDebugTool::ShowWords
	bool ___ShowWords_5;
	// System.Boolean TMPro.Examples.TMP_TextInfoDebugTool::ShowLinks
	bool ___ShowLinks_6;
	// System.Boolean TMPro.Examples.TMP_TextInfoDebugTool::ShowLines
	bool ___ShowLines_7;
	// System.Boolean TMPro.Examples.TMP_TextInfoDebugTool::ShowMeshBounds
	bool ___ShowMeshBounds_8;
	// System.Boolean TMPro.Examples.TMP_TextInfoDebugTool::ShowTextBounds
	bool ___ShowTextBounds_9;
	// System.String TMPro.Examples.TMP_TextInfoDebugTool::ObjectStats
	String_t* ___ObjectStats_10;
	// TMPro.TMP_Text TMPro.Examples.TMP_TextInfoDebugTool::m_TextComponent
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * ___m_TextComponent_11;
	// UnityEngine.Transform TMPro.Examples.TMP_TextInfoDebugTool::m_Transform
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___m_Transform_12;

public:
	inline static int32_t get_offset_of_ShowCharacters_4() { return static_cast<int32_t>(offsetof(TMP_TextInfoDebugTool_t327D46DA1CC9A18738A134F109EA5AD3F8437FE7, ___ShowCharacters_4)); }
	inline bool get_ShowCharacters_4() const { return ___ShowCharacters_4; }
	inline bool* get_address_of_ShowCharacters_4() { return &___ShowCharacters_4; }
	inline void set_ShowCharacters_4(bool value)
	{
		___ShowCharacters_4 = value;
	}

	inline static int32_t get_offset_of_ShowWords_5() { return static_cast<int32_t>(offsetof(TMP_TextInfoDebugTool_t327D46DA1CC9A18738A134F109EA5AD3F8437FE7, ___ShowWords_5)); }
	inline bool get_ShowWords_5() const { return ___ShowWords_5; }
	inline bool* get_address_of_ShowWords_5() { return &___ShowWords_5; }
	inline void set_ShowWords_5(bool value)
	{
		___ShowWords_5 = value;
	}

	inline static int32_t get_offset_of_ShowLinks_6() { return static_cast<int32_t>(offsetof(TMP_TextInfoDebugTool_t327D46DA1CC9A18738A134F109EA5AD3F8437FE7, ___ShowLinks_6)); }
	inline bool get_ShowLinks_6() const { return ___ShowLinks_6; }
	inline bool* get_address_of_ShowLinks_6() { return &___ShowLinks_6; }
	inline void set_ShowLinks_6(bool value)
	{
		___ShowLinks_6 = value;
	}

	inline static int32_t get_offset_of_ShowLines_7() { return static_cast<int32_t>(offsetof(TMP_TextInfoDebugTool_t327D46DA1CC9A18738A134F109EA5AD3F8437FE7, ___ShowLines_7)); }
	inline bool get_ShowLines_7() const { return ___ShowLines_7; }
	inline bool* get_address_of_ShowLines_7() { return &___ShowLines_7; }
	inline void set_ShowLines_7(bool value)
	{
		___ShowLines_7 = value;
	}

	inline static int32_t get_offset_of_ShowMeshBounds_8() { return static_cast<int32_t>(offsetof(TMP_TextInfoDebugTool_t327D46DA1CC9A18738A134F109EA5AD3F8437FE7, ___ShowMeshBounds_8)); }
	inline bool get_ShowMeshBounds_8() const { return ___ShowMeshBounds_8; }
	inline bool* get_address_of_ShowMeshBounds_8() { return &___ShowMeshBounds_8; }
	inline void set_ShowMeshBounds_8(bool value)
	{
		___ShowMeshBounds_8 = value;
	}

	inline static int32_t get_offset_of_ShowTextBounds_9() { return static_cast<int32_t>(offsetof(TMP_TextInfoDebugTool_t327D46DA1CC9A18738A134F109EA5AD3F8437FE7, ___ShowTextBounds_9)); }
	inline bool get_ShowTextBounds_9() const { return ___ShowTextBounds_9; }
	inline bool* get_address_of_ShowTextBounds_9() { return &___ShowTextBounds_9; }
	inline void set_ShowTextBounds_9(bool value)
	{
		___ShowTextBounds_9 = value;
	}

	inline static int32_t get_offset_of_ObjectStats_10() { return static_cast<int32_t>(offsetof(TMP_TextInfoDebugTool_t327D46DA1CC9A18738A134F109EA5AD3F8437FE7, ___ObjectStats_10)); }
	inline String_t* get_ObjectStats_10() const { return ___ObjectStats_10; }
	inline String_t** get_address_of_ObjectStats_10() { return &___ObjectStats_10; }
	inline void set_ObjectStats_10(String_t* value)
	{
		___ObjectStats_10 = value;
		Il2CppCodeGenWriteBarrier((&___ObjectStats_10), value);
	}

	inline static int32_t get_offset_of_m_TextComponent_11() { return static_cast<int32_t>(offsetof(TMP_TextInfoDebugTool_t327D46DA1CC9A18738A134F109EA5AD3F8437FE7, ___m_TextComponent_11)); }
	inline TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * get_m_TextComponent_11() const { return ___m_TextComponent_11; }
	inline TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 ** get_address_of_m_TextComponent_11() { return &___m_TextComponent_11; }
	inline void set_m_TextComponent_11(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * value)
	{
		___m_TextComponent_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextComponent_11), value);
	}

	inline static int32_t get_offset_of_m_Transform_12() { return static_cast<int32_t>(offsetof(TMP_TextInfoDebugTool_t327D46DA1CC9A18738A134F109EA5AD3F8437FE7, ___m_Transform_12)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_m_Transform_12() const { return ___m_Transform_12; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_m_Transform_12() { return &___m_Transform_12; }
	inline void set_m_Transform_12(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___m_Transform_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_Transform_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_TEXTINFODEBUGTOOL_T327D46DA1CC9A18738A134F109EA5AD3F8437FE7_H
#ifndef TMP_TEXTSELECTOR_A_T1931459985F680024B2C3E2E27919BE367243C6E_H
#define TMP_TEXTSELECTOR_A_T1931459985F680024B2C3E2E27919BE367243C6E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TMP_TextSelector_A
struct  TMP_TextSelector_A_t1931459985F680024B2C3E2E27919BE367243C6E  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// TMPro.TextMeshPro TMPro.Examples.TMP_TextSelector_A::m_TextMeshPro
	TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2 * ___m_TextMeshPro_4;
	// UnityEngine.Camera TMPro.Examples.TMP_TextSelector_A::m_Camera
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___m_Camera_5;
	// System.Boolean TMPro.Examples.TMP_TextSelector_A::m_isHoveringObject
	bool ___m_isHoveringObject_6;
	// System.Int32 TMPro.Examples.TMP_TextSelector_A::m_selectedLink
	int32_t ___m_selectedLink_7;
	// System.Int32 TMPro.Examples.TMP_TextSelector_A::m_lastCharIndex
	int32_t ___m_lastCharIndex_8;
	// System.Int32 TMPro.Examples.TMP_TextSelector_A::m_lastWordIndex
	int32_t ___m_lastWordIndex_9;

public:
	inline static int32_t get_offset_of_m_TextMeshPro_4() { return static_cast<int32_t>(offsetof(TMP_TextSelector_A_t1931459985F680024B2C3E2E27919BE367243C6E, ___m_TextMeshPro_4)); }
	inline TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2 * get_m_TextMeshPro_4() const { return ___m_TextMeshPro_4; }
	inline TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2 ** get_address_of_m_TextMeshPro_4() { return &___m_TextMeshPro_4; }
	inline void set_m_TextMeshPro_4(TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2 * value)
	{
		___m_TextMeshPro_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextMeshPro_4), value);
	}

	inline static int32_t get_offset_of_m_Camera_5() { return static_cast<int32_t>(offsetof(TMP_TextSelector_A_t1931459985F680024B2C3E2E27919BE367243C6E, ___m_Camera_5)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get_m_Camera_5() const { return ___m_Camera_5; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of_m_Camera_5() { return &___m_Camera_5; }
	inline void set_m_Camera_5(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		___m_Camera_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Camera_5), value);
	}

	inline static int32_t get_offset_of_m_isHoveringObject_6() { return static_cast<int32_t>(offsetof(TMP_TextSelector_A_t1931459985F680024B2C3E2E27919BE367243C6E, ___m_isHoveringObject_6)); }
	inline bool get_m_isHoveringObject_6() const { return ___m_isHoveringObject_6; }
	inline bool* get_address_of_m_isHoveringObject_6() { return &___m_isHoveringObject_6; }
	inline void set_m_isHoveringObject_6(bool value)
	{
		___m_isHoveringObject_6 = value;
	}

	inline static int32_t get_offset_of_m_selectedLink_7() { return static_cast<int32_t>(offsetof(TMP_TextSelector_A_t1931459985F680024B2C3E2E27919BE367243C6E, ___m_selectedLink_7)); }
	inline int32_t get_m_selectedLink_7() const { return ___m_selectedLink_7; }
	inline int32_t* get_address_of_m_selectedLink_7() { return &___m_selectedLink_7; }
	inline void set_m_selectedLink_7(int32_t value)
	{
		___m_selectedLink_7 = value;
	}

	inline static int32_t get_offset_of_m_lastCharIndex_8() { return static_cast<int32_t>(offsetof(TMP_TextSelector_A_t1931459985F680024B2C3E2E27919BE367243C6E, ___m_lastCharIndex_8)); }
	inline int32_t get_m_lastCharIndex_8() const { return ___m_lastCharIndex_8; }
	inline int32_t* get_address_of_m_lastCharIndex_8() { return &___m_lastCharIndex_8; }
	inline void set_m_lastCharIndex_8(int32_t value)
	{
		___m_lastCharIndex_8 = value;
	}

	inline static int32_t get_offset_of_m_lastWordIndex_9() { return static_cast<int32_t>(offsetof(TMP_TextSelector_A_t1931459985F680024B2C3E2E27919BE367243C6E, ___m_lastWordIndex_9)); }
	inline int32_t get_m_lastWordIndex_9() const { return ___m_lastWordIndex_9; }
	inline int32_t* get_address_of_m_lastWordIndex_9() { return &___m_lastWordIndex_9; }
	inline void set_m_lastWordIndex_9(int32_t value)
	{
		___m_lastWordIndex_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_TEXTSELECTOR_A_T1931459985F680024B2C3E2E27919BE367243C6E_H
#ifndef TMP_TEXTSELECTOR_B_T9BD007A30F11CFDAB04DFD4FA6769209EC8BF44B_H
#define TMP_TEXTSELECTOR_B_T9BD007A30F11CFDAB04DFD4FA6769209EC8BF44B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TMP_TextSelector_B
struct  TMP_TextSelector_B_t9BD007A30F11CFDAB04DFD4FA6769209EC8BF44B  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.RectTransform TMPro.Examples.TMP_TextSelector_B::TextPopup_Prefab_01
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___TextPopup_Prefab_01_4;
	// UnityEngine.RectTransform TMPro.Examples.TMP_TextSelector_B::m_TextPopup_RectTransform
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___m_TextPopup_RectTransform_5;
	// TMPro.TextMeshProUGUI TMPro.Examples.TMP_TextSelector_B::m_TextPopup_TMPComponent
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ___m_TextPopup_TMPComponent_6;
	// TMPro.TextMeshProUGUI TMPro.Examples.TMP_TextSelector_B::m_TextMeshPro
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ___m_TextMeshPro_9;
	// UnityEngine.Canvas TMPro.Examples.TMP_TextSelector_B::m_Canvas
	Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * ___m_Canvas_10;
	// UnityEngine.Camera TMPro.Examples.TMP_TextSelector_B::m_Camera
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___m_Camera_11;
	// System.Boolean TMPro.Examples.TMP_TextSelector_B::isHoveringObject
	bool ___isHoveringObject_12;
	// System.Int32 TMPro.Examples.TMP_TextSelector_B::m_selectedWord
	int32_t ___m_selectedWord_13;
	// System.Int32 TMPro.Examples.TMP_TextSelector_B::m_selectedLink
	int32_t ___m_selectedLink_14;
	// System.Int32 TMPro.Examples.TMP_TextSelector_B::m_lastIndex
	int32_t ___m_lastIndex_15;
	// UnityEngine.Matrix4x4 TMPro.Examples.TMP_TextSelector_B::m_matrix
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___m_matrix_16;
	// TMPro.TMP_MeshInfo[] TMPro.Examples.TMP_TextSelector_B::m_cachedMeshInfoVertexData
	TMP_MeshInfoU5BU5D_t7F7564862ADABD75DAD9B09FF274591F807FFDE9* ___m_cachedMeshInfoVertexData_17;

public:
	inline static int32_t get_offset_of_TextPopup_Prefab_01_4() { return static_cast<int32_t>(offsetof(TMP_TextSelector_B_t9BD007A30F11CFDAB04DFD4FA6769209EC8BF44B, ___TextPopup_Prefab_01_4)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_TextPopup_Prefab_01_4() const { return ___TextPopup_Prefab_01_4; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_TextPopup_Prefab_01_4() { return &___TextPopup_Prefab_01_4; }
	inline void set_TextPopup_Prefab_01_4(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___TextPopup_Prefab_01_4 = value;
		Il2CppCodeGenWriteBarrier((&___TextPopup_Prefab_01_4), value);
	}

	inline static int32_t get_offset_of_m_TextPopup_RectTransform_5() { return static_cast<int32_t>(offsetof(TMP_TextSelector_B_t9BD007A30F11CFDAB04DFD4FA6769209EC8BF44B, ___m_TextPopup_RectTransform_5)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_m_TextPopup_RectTransform_5() const { return ___m_TextPopup_RectTransform_5; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_m_TextPopup_RectTransform_5() { return &___m_TextPopup_RectTransform_5; }
	inline void set_m_TextPopup_RectTransform_5(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___m_TextPopup_RectTransform_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextPopup_RectTransform_5), value);
	}

	inline static int32_t get_offset_of_m_TextPopup_TMPComponent_6() { return static_cast<int32_t>(offsetof(TMP_TextSelector_B_t9BD007A30F11CFDAB04DFD4FA6769209EC8BF44B, ___m_TextPopup_TMPComponent_6)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get_m_TextPopup_TMPComponent_6() const { return ___m_TextPopup_TMPComponent_6; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of_m_TextPopup_TMPComponent_6() { return &___m_TextPopup_TMPComponent_6; }
	inline void set_m_TextPopup_TMPComponent_6(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		___m_TextPopup_TMPComponent_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextPopup_TMPComponent_6), value);
	}

	inline static int32_t get_offset_of_m_TextMeshPro_9() { return static_cast<int32_t>(offsetof(TMP_TextSelector_B_t9BD007A30F11CFDAB04DFD4FA6769209EC8BF44B, ___m_TextMeshPro_9)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get_m_TextMeshPro_9() const { return ___m_TextMeshPro_9; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of_m_TextMeshPro_9() { return &___m_TextMeshPro_9; }
	inline void set_m_TextMeshPro_9(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		___m_TextMeshPro_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextMeshPro_9), value);
	}

	inline static int32_t get_offset_of_m_Canvas_10() { return static_cast<int32_t>(offsetof(TMP_TextSelector_B_t9BD007A30F11CFDAB04DFD4FA6769209EC8BF44B, ___m_Canvas_10)); }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * get_m_Canvas_10() const { return ___m_Canvas_10; }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 ** get_address_of_m_Canvas_10() { return &___m_Canvas_10; }
	inline void set_m_Canvas_10(Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * value)
	{
		___m_Canvas_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_Canvas_10), value);
	}

	inline static int32_t get_offset_of_m_Camera_11() { return static_cast<int32_t>(offsetof(TMP_TextSelector_B_t9BD007A30F11CFDAB04DFD4FA6769209EC8BF44B, ___m_Camera_11)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get_m_Camera_11() const { return ___m_Camera_11; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of_m_Camera_11() { return &___m_Camera_11; }
	inline void set_m_Camera_11(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		___m_Camera_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_Camera_11), value);
	}

	inline static int32_t get_offset_of_isHoveringObject_12() { return static_cast<int32_t>(offsetof(TMP_TextSelector_B_t9BD007A30F11CFDAB04DFD4FA6769209EC8BF44B, ___isHoveringObject_12)); }
	inline bool get_isHoveringObject_12() const { return ___isHoveringObject_12; }
	inline bool* get_address_of_isHoveringObject_12() { return &___isHoveringObject_12; }
	inline void set_isHoveringObject_12(bool value)
	{
		___isHoveringObject_12 = value;
	}

	inline static int32_t get_offset_of_m_selectedWord_13() { return static_cast<int32_t>(offsetof(TMP_TextSelector_B_t9BD007A30F11CFDAB04DFD4FA6769209EC8BF44B, ___m_selectedWord_13)); }
	inline int32_t get_m_selectedWord_13() const { return ___m_selectedWord_13; }
	inline int32_t* get_address_of_m_selectedWord_13() { return &___m_selectedWord_13; }
	inline void set_m_selectedWord_13(int32_t value)
	{
		___m_selectedWord_13 = value;
	}

	inline static int32_t get_offset_of_m_selectedLink_14() { return static_cast<int32_t>(offsetof(TMP_TextSelector_B_t9BD007A30F11CFDAB04DFD4FA6769209EC8BF44B, ___m_selectedLink_14)); }
	inline int32_t get_m_selectedLink_14() const { return ___m_selectedLink_14; }
	inline int32_t* get_address_of_m_selectedLink_14() { return &___m_selectedLink_14; }
	inline void set_m_selectedLink_14(int32_t value)
	{
		___m_selectedLink_14 = value;
	}

	inline static int32_t get_offset_of_m_lastIndex_15() { return static_cast<int32_t>(offsetof(TMP_TextSelector_B_t9BD007A30F11CFDAB04DFD4FA6769209EC8BF44B, ___m_lastIndex_15)); }
	inline int32_t get_m_lastIndex_15() const { return ___m_lastIndex_15; }
	inline int32_t* get_address_of_m_lastIndex_15() { return &___m_lastIndex_15; }
	inline void set_m_lastIndex_15(int32_t value)
	{
		___m_lastIndex_15 = value;
	}

	inline static int32_t get_offset_of_m_matrix_16() { return static_cast<int32_t>(offsetof(TMP_TextSelector_B_t9BD007A30F11CFDAB04DFD4FA6769209EC8BF44B, ___m_matrix_16)); }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  get_m_matrix_16() const { return ___m_matrix_16; }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * get_address_of_m_matrix_16() { return &___m_matrix_16; }
	inline void set_m_matrix_16(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  value)
	{
		___m_matrix_16 = value;
	}

	inline static int32_t get_offset_of_m_cachedMeshInfoVertexData_17() { return static_cast<int32_t>(offsetof(TMP_TextSelector_B_t9BD007A30F11CFDAB04DFD4FA6769209EC8BF44B, ___m_cachedMeshInfoVertexData_17)); }
	inline TMP_MeshInfoU5BU5D_t7F7564862ADABD75DAD9B09FF274591F807FFDE9* get_m_cachedMeshInfoVertexData_17() const { return ___m_cachedMeshInfoVertexData_17; }
	inline TMP_MeshInfoU5BU5D_t7F7564862ADABD75DAD9B09FF274591F807FFDE9** get_address_of_m_cachedMeshInfoVertexData_17() { return &___m_cachedMeshInfoVertexData_17; }
	inline void set_m_cachedMeshInfoVertexData_17(TMP_MeshInfoU5BU5D_t7F7564862ADABD75DAD9B09FF274591F807FFDE9* value)
	{
		___m_cachedMeshInfoVertexData_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_cachedMeshInfoVertexData_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_TEXTSELECTOR_B_T9BD007A30F11CFDAB04DFD4FA6769209EC8BF44B_H
#ifndef TMP_UIFRAMERATECOUNTER_TA4B70430361CC81F922192640261ACA474A98C20_H
#define TMP_UIFRAMERATECOUNTER_TA4B70430361CC81F922192640261ACA474A98C20_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TMP_UiFrameRateCounter
struct  TMP_UiFrameRateCounter_tA4B70430361CC81F922192640261ACA474A98C20  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single TMPro.Examples.TMP_UiFrameRateCounter::UpdateInterval
	float ___UpdateInterval_4;
	// System.Single TMPro.Examples.TMP_UiFrameRateCounter::m_LastInterval
	float ___m_LastInterval_5;
	// System.Int32 TMPro.Examples.TMP_UiFrameRateCounter::m_Frames
	int32_t ___m_Frames_6;
	// TMPro.Examples.TMP_UiFrameRateCounter/FpsCounterAnchorPositions TMPro.Examples.TMP_UiFrameRateCounter::AnchorPosition
	int32_t ___AnchorPosition_7;
	// System.String TMPro.Examples.TMP_UiFrameRateCounter::htmlColorTag
	String_t* ___htmlColorTag_8;
	// TMPro.TextMeshProUGUI TMPro.Examples.TMP_UiFrameRateCounter::m_TextMeshPro
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ___m_TextMeshPro_10;
	// UnityEngine.RectTransform TMPro.Examples.TMP_UiFrameRateCounter::m_frameCounter_transform
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___m_frameCounter_transform_11;
	// TMPro.Examples.TMP_UiFrameRateCounter/FpsCounterAnchorPositions TMPro.Examples.TMP_UiFrameRateCounter::last_AnchorPosition
	int32_t ___last_AnchorPosition_12;

public:
	inline static int32_t get_offset_of_UpdateInterval_4() { return static_cast<int32_t>(offsetof(TMP_UiFrameRateCounter_tA4B70430361CC81F922192640261ACA474A98C20, ___UpdateInterval_4)); }
	inline float get_UpdateInterval_4() const { return ___UpdateInterval_4; }
	inline float* get_address_of_UpdateInterval_4() { return &___UpdateInterval_4; }
	inline void set_UpdateInterval_4(float value)
	{
		___UpdateInterval_4 = value;
	}

	inline static int32_t get_offset_of_m_LastInterval_5() { return static_cast<int32_t>(offsetof(TMP_UiFrameRateCounter_tA4B70430361CC81F922192640261ACA474A98C20, ___m_LastInterval_5)); }
	inline float get_m_LastInterval_5() const { return ___m_LastInterval_5; }
	inline float* get_address_of_m_LastInterval_5() { return &___m_LastInterval_5; }
	inline void set_m_LastInterval_5(float value)
	{
		___m_LastInterval_5 = value;
	}

	inline static int32_t get_offset_of_m_Frames_6() { return static_cast<int32_t>(offsetof(TMP_UiFrameRateCounter_tA4B70430361CC81F922192640261ACA474A98C20, ___m_Frames_6)); }
	inline int32_t get_m_Frames_6() const { return ___m_Frames_6; }
	inline int32_t* get_address_of_m_Frames_6() { return &___m_Frames_6; }
	inline void set_m_Frames_6(int32_t value)
	{
		___m_Frames_6 = value;
	}

	inline static int32_t get_offset_of_AnchorPosition_7() { return static_cast<int32_t>(offsetof(TMP_UiFrameRateCounter_tA4B70430361CC81F922192640261ACA474A98C20, ___AnchorPosition_7)); }
	inline int32_t get_AnchorPosition_7() const { return ___AnchorPosition_7; }
	inline int32_t* get_address_of_AnchorPosition_7() { return &___AnchorPosition_7; }
	inline void set_AnchorPosition_7(int32_t value)
	{
		___AnchorPosition_7 = value;
	}

	inline static int32_t get_offset_of_htmlColorTag_8() { return static_cast<int32_t>(offsetof(TMP_UiFrameRateCounter_tA4B70430361CC81F922192640261ACA474A98C20, ___htmlColorTag_8)); }
	inline String_t* get_htmlColorTag_8() const { return ___htmlColorTag_8; }
	inline String_t** get_address_of_htmlColorTag_8() { return &___htmlColorTag_8; }
	inline void set_htmlColorTag_8(String_t* value)
	{
		___htmlColorTag_8 = value;
		Il2CppCodeGenWriteBarrier((&___htmlColorTag_8), value);
	}

	inline static int32_t get_offset_of_m_TextMeshPro_10() { return static_cast<int32_t>(offsetof(TMP_UiFrameRateCounter_tA4B70430361CC81F922192640261ACA474A98C20, ___m_TextMeshPro_10)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get_m_TextMeshPro_10() const { return ___m_TextMeshPro_10; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of_m_TextMeshPro_10() { return &___m_TextMeshPro_10; }
	inline void set_m_TextMeshPro_10(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		___m_TextMeshPro_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextMeshPro_10), value);
	}

	inline static int32_t get_offset_of_m_frameCounter_transform_11() { return static_cast<int32_t>(offsetof(TMP_UiFrameRateCounter_tA4B70430361CC81F922192640261ACA474A98C20, ___m_frameCounter_transform_11)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_m_frameCounter_transform_11() const { return ___m_frameCounter_transform_11; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_m_frameCounter_transform_11() { return &___m_frameCounter_transform_11; }
	inline void set_m_frameCounter_transform_11(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___m_frameCounter_transform_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_frameCounter_transform_11), value);
	}

	inline static int32_t get_offset_of_last_AnchorPosition_12() { return static_cast<int32_t>(offsetof(TMP_UiFrameRateCounter_tA4B70430361CC81F922192640261ACA474A98C20, ___last_AnchorPosition_12)); }
	inline int32_t get_last_AnchorPosition_12() const { return ___last_AnchorPosition_12; }
	inline int32_t* get_address_of_last_AnchorPosition_12() { return &___last_AnchorPosition_12; }
	inline void set_last_AnchorPosition_12(int32_t value)
	{
		___last_AnchorPosition_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_UIFRAMERATECOUNTER_TA4B70430361CC81F922192640261ACA474A98C20_H
#ifndef TMPRO_INSTRUCTIONOVERLAY_T0951D0394E58954E16D604924BD2FD0110A3E31E_H
#define TMPRO_INSTRUCTIONOVERLAY_T0951D0394E58954E16D604924BD2FD0110A3E31E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TMPro_InstructionOverlay
struct  TMPro_InstructionOverlay_t0951D0394E58954E16D604924BD2FD0110A3E31E  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// TMPro.Examples.TMPro_InstructionOverlay/FpsCounterAnchorPositions TMPro.Examples.TMPro_InstructionOverlay::AnchorPosition
	int32_t ___AnchorPosition_4;
	// TMPro.TextMeshPro TMPro.Examples.TMPro_InstructionOverlay::m_TextMeshPro
	TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2 * ___m_TextMeshPro_6;
	// TMPro.TextContainer TMPro.Examples.TMPro_InstructionOverlay::m_textContainer
	TextContainer_tF5E5EB56D152102B19C27607F84847CA594391DE * ___m_textContainer_7;
	// UnityEngine.Transform TMPro.Examples.TMPro_InstructionOverlay::m_frameCounter_transform
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___m_frameCounter_transform_8;
	// UnityEngine.Camera TMPro.Examples.TMPro_InstructionOverlay::m_camera
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___m_camera_9;

public:
	inline static int32_t get_offset_of_AnchorPosition_4() { return static_cast<int32_t>(offsetof(TMPro_InstructionOverlay_t0951D0394E58954E16D604924BD2FD0110A3E31E, ___AnchorPosition_4)); }
	inline int32_t get_AnchorPosition_4() const { return ___AnchorPosition_4; }
	inline int32_t* get_address_of_AnchorPosition_4() { return &___AnchorPosition_4; }
	inline void set_AnchorPosition_4(int32_t value)
	{
		___AnchorPosition_4 = value;
	}

	inline static int32_t get_offset_of_m_TextMeshPro_6() { return static_cast<int32_t>(offsetof(TMPro_InstructionOverlay_t0951D0394E58954E16D604924BD2FD0110A3E31E, ___m_TextMeshPro_6)); }
	inline TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2 * get_m_TextMeshPro_6() const { return ___m_TextMeshPro_6; }
	inline TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2 ** get_address_of_m_TextMeshPro_6() { return &___m_TextMeshPro_6; }
	inline void set_m_TextMeshPro_6(TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2 * value)
	{
		___m_TextMeshPro_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextMeshPro_6), value);
	}

	inline static int32_t get_offset_of_m_textContainer_7() { return static_cast<int32_t>(offsetof(TMPro_InstructionOverlay_t0951D0394E58954E16D604924BD2FD0110A3E31E, ___m_textContainer_7)); }
	inline TextContainer_tF5E5EB56D152102B19C27607F84847CA594391DE * get_m_textContainer_7() const { return ___m_textContainer_7; }
	inline TextContainer_tF5E5EB56D152102B19C27607F84847CA594391DE ** get_address_of_m_textContainer_7() { return &___m_textContainer_7; }
	inline void set_m_textContainer_7(TextContainer_tF5E5EB56D152102B19C27607F84847CA594391DE * value)
	{
		___m_textContainer_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_textContainer_7), value);
	}

	inline static int32_t get_offset_of_m_frameCounter_transform_8() { return static_cast<int32_t>(offsetof(TMPro_InstructionOverlay_t0951D0394E58954E16D604924BD2FD0110A3E31E, ___m_frameCounter_transform_8)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_m_frameCounter_transform_8() const { return ___m_frameCounter_transform_8; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_m_frameCounter_transform_8() { return &___m_frameCounter_transform_8; }
	inline void set_m_frameCounter_transform_8(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___m_frameCounter_transform_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_frameCounter_transform_8), value);
	}

	inline static int32_t get_offset_of_m_camera_9() { return static_cast<int32_t>(offsetof(TMPro_InstructionOverlay_t0951D0394E58954E16D604924BD2FD0110A3E31E, ___m_camera_9)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get_m_camera_9() const { return ___m_camera_9; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of_m_camera_9() { return &___m_camera_9; }
	inline void set_m_camera_9(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		___m_camera_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_camera_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMPRO_INSTRUCTIONOVERLAY_T0951D0394E58954E16D604924BD2FD0110A3E31E_H
#ifndef TEXTMESHSPAWNER_T7A50739A3F9750469C9F130607DA20D66CA22686_H
#define TEXTMESHSPAWNER_T7A50739A3F9750469C9F130607DA20D66CA22686_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TextMeshSpawner
struct  TextMeshSpawner_t7A50739A3F9750469C9F130607DA20D66CA22686  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Int32 TMPro.Examples.TextMeshSpawner::SpawnType
	int32_t ___SpawnType_4;
	// System.Int32 TMPro.Examples.TextMeshSpawner::NumberOfNPC
	int32_t ___NumberOfNPC_5;
	// UnityEngine.Font TMPro.Examples.TextMeshSpawner::TheFont
	Font_t1EDE54AF557272BE314EB4B40EFA50CEB353CA26 * ___TheFont_6;
	// TMPro.Examples.TextMeshProFloatingText TMPro.Examples.TextMeshSpawner::floatingText_Script
	TextMeshProFloatingText_tD52C5B081FC480F24349C2C0F5099F0693EEC913 * ___floatingText_Script_7;

public:
	inline static int32_t get_offset_of_SpawnType_4() { return static_cast<int32_t>(offsetof(TextMeshSpawner_t7A50739A3F9750469C9F130607DA20D66CA22686, ___SpawnType_4)); }
	inline int32_t get_SpawnType_4() const { return ___SpawnType_4; }
	inline int32_t* get_address_of_SpawnType_4() { return &___SpawnType_4; }
	inline void set_SpawnType_4(int32_t value)
	{
		___SpawnType_4 = value;
	}

	inline static int32_t get_offset_of_NumberOfNPC_5() { return static_cast<int32_t>(offsetof(TextMeshSpawner_t7A50739A3F9750469C9F130607DA20D66CA22686, ___NumberOfNPC_5)); }
	inline int32_t get_NumberOfNPC_5() const { return ___NumberOfNPC_5; }
	inline int32_t* get_address_of_NumberOfNPC_5() { return &___NumberOfNPC_5; }
	inline void set_NumberOfNPC_5(int32_t value)
	{
		___NumberOfNPC_5 = value;
	}

	inline static int32_t get_offset_of_TheFont_6() { return static_cast<int32_t>(offsetof(TextMeshSpawner_t7A50739A3F9750469C9F130607DA20D66CA22686, ___TheFont_6)); }
	inline Font_t1EDE54AF557272BE314EB4B40EFA50CEB353CA26 * get_TheFont_6() const { return ___TheFont_6; }
	inline Font_t1EDE54AF557272BE314EB4B40EFA50CEB353CA26 ** get_address_of_TheFont_6() { return &___TheFont_6; }
	inline void set_TheFont_6(Font_t1EDE54AF557272BE314EB4B40EFA50CEB353CA26 * value)
	{
		___TheFont_6 = value;
		Il2CppCodeGenWriteBarrier((&___TheFont_6), value);
	}

	inline static int32_t get_offset_of_floatingText_Script_7() { return static_cast<int32_t>(offsetof(TextMeshSpawner_t7A50739A3F9750469C9F130607DA20D66CA22686, ___floatingText_Script_7)); }
	inline TextMeshProFloatingText_tD52C5B081FC480F24349C2C0F5099F0693EEC913 * get_floatingText_Script_7() const { return ___floatingText_Script_7; }
	inline TextMeshProFloatingText_tD52C5B081FC480F24349C2C0F5099F0693EEC913 ** get_address_of_floatingText_Script_7() { return &___floatingText_Script_7; }
	inline void set_floatingText_Script_7(TextMeshProFloatingText_tD52C5B081FC480F24349C2C0F5099F0693EEC913 * value)
	{
		___floatingText_Script_7 = value;
		Il2CppCodeGenWriteBarrier((&___floatingText_Script_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTMESHSPAWNER_T7A50739A3F9750469C9F130607DA20D66CA22686_H
#ifndef VERTEXCOLORCYCLER_T5EEC79CC79AA4B00DF4BE5CE390C12E47385299C_H
#define VERTEXCOLORCYCLER_T5EEC79CC79AA4B00DF4BE5CE390C12E47385299C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.VertexColorCycler
struct  VertexColorCycler_t5EEC79CC79AA4B00DF4BE5CE390C12E47385299C  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// TMPro.TMP_Text TMPro.Examples.VertexColorCycler::m_TextComponent
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * ___m_TextComponent_4;

public:
	inline static int32_t get_offset_of_m_TextComponent_4() { return static_cast<int32_t>(offsetof(VertexColorCycler_t5EEC79CC79AA4B00DF4BE5CE390C12E47385299C, ___m_TextComponent_4)); }
	inline TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * get_m_TextComponent_4() const { return ___m_TextComponent_4; }
	inline TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 ** get_address_of_m_TextComponent_4() { return &___m_TextComponent_4; }
	inline void set_m_TextComponent_4(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * value)
	{
		___m_TextComponent_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextComponent_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTEXCOLORCYCLER_T5EEC79CC79AA4B00DF4BE5CE390C12E47385299C_H
#ifndef VERTEXJITTER_TF4F1AE4AF51C09E6FE57F91227A228B61BD0ACF0_H
#define VERTEXJITTER_TF4F1AE4AF51C09E6FE57F91227A228B61BD0ACF0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.VertexJitter
struct  VertexJitter_tF4F1AE4AF51C09E6FE57F91227A228B61BD0ACF0  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single TMPro.Examples.VertexJitter::AngleMultiplier
	float ___AngleMultiplier_4;
	// System.Single TMPro.Examples.VertexJitter::SpeedMultiplier
	float ___SpeedMultiplier_5;
	// System.Single TMPro.Examples.VertexJitter::CurveScale
	float ___CurveScale_6;
	// TMPro.TMP_Text TMPro.Examples.VertexJitter::m_TextComponent
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * ___m_TextComponent_7;
	// System.Boolean TMPro.Examples.VertexJitter::hasTextChanged
	bool ___hasTextChanged_8;

public:
	inline static int32_t get_offset_of_AngleMultiplier_4() { return static_cast<int32_t>(offsetof(VertexJitter_tF4F1AE4AF51C09E6FE57F91227A228B61BD0ACF0, ___AngleMultiplier_4)); }
	inline float get_AngleMultiplier_4() const { return ___AngleMultiplier_4; }
	inline float* get_address_of_AngleMultiplier_4() { return &___AngleMultiplier_4; }
	inline void set_AngleMultiplier_4(float value)
	{
		___AngleMultiplier_4 = value;
	}

	inline static int32_t get_offset_of_SpeedMultiplier_5() { return static_cast<int32_t>(offsetof(VertexJitter_tF4F1AE4AF51C09E6FE57F91227A228B61BD0ACF0, ___SpeedMultiplier_5)); }
	inline float get_SpeedMultiplier_5() const { return ___SpeedMultiplier_5; }
	inline float* get_address_of_SpeedMultiplier_5() { return &___SpeedMultiplier_5; }
	inline void set_SpeedMultiplier_5(float value)
	{
		___SpeedMultiplier_5 = value;
	}

	inline static int32_t get_offset_of_CurveScale_6() { return static_cast<int32_t>(offsetof(VertexJitter_tF4F1AE4AF51C09E6FE57F91227A228B61BD0ACF0, ___CurveScale_6)); }
	inline float get_CurveScale_6() const { return ___CurveScale_6; }
	inline float* get_address_of_CurveScale_6() { return &___CurveScale_6; }
	inline void set_CurveScale_6(float value)
	{
		___CurveScale_6 = value;
	}

	inline static int32_t get_offset_of_m_TextComponent_7() { return static_cast<int32_t>(offsetof(VertexJitter_tF4F1AE4AF51C09E6FE57F91227A228B61BD0ACF0, ___m_TextComponent_7)); }
	inline TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * get_m_TextComponent_7() const { return ___m_TextComponent_7; }
	inline TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 ** get_address_of_m_TextComponent_7() { return &___m_TextComponent_7; }
	inline void set_m_TextComponent_7(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * value)
	{
		___m_TextComponent_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextComponent_7), value);
	}

	inline static int32_t get_offset_of_hasTextChanged_8() { return static_cast<int32_t>(offsetof(VertexJitter_tF4F1AE4AF51C09E6FE57F91227A228B61BD0ACF0, ___hasTextChanged_8)); }
	inline bool get_hasTextChanged_8() const { return ___hasTextChanged_8; }
	inline bool* get_address_of_hasTextChanged_8() { return &___hasTextChanged_8; }
	inline void set_hasTextChanged_8(bool value)
	{
		___hasTextChanged_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTEXJITTER_TF4F1AE4AF51C09E6FE57F91227A228B61BD0ACF0_H
#ifndef VERTEXSHAKEA_TCBD5B7397DD332038CE795E6B0A4D5B199739C27_H
#define VERTEXSHAKEA_TCBD5B7397DD332038CE795E6B0A4D5B199739C27_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.VertexShakeA
struct  VertexShakeA_tCBD5B7397DD332038CE795E6B0A4D5B199739C27  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single TMPro.Examples.VertexShakeA::AngleMultiplier
	float ___AngleMultiplier_4;
	// System.Single TMPro.Examples.VertexShakeA::SpeedMultiplier
	float ___SpeedMultiplier_5;
	// System.Single TMPro.Examples.VertexShakeA::ScaleMultiplier
	float ___ScaleMultiplier_6;
	// System.Single TMPro.Examples.VertexShakeA::RotationMultiplier
	float ___RotationMultiplier_7;
	// TMPro.TMP_Text TMPro.Examples.VertexShakeA::m_TextComponent
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * ___m_TextComponent_8;
	// System.Boolean TMPro.Examples.VertexShakeA::hasTextChanged
	bool ___hasTextChanged_9;

public:
	inline static int32_t get_offset_of_AngleMultiplier_4() { return static_cast<int32_t>(offsetof(VertexShakeA_tCBD5B7397DD332038CE795E6B0A4D5B199739C27, ___AngleMultiplier_4)); }
	inline float get_AngleMultiplier_4() const { return ___AngleMultiplier_4; }
	inline float* get_address_of_AngleMultiplier_4() { return &___AngleMultiplier_4; }
	inline void set_AngleMultiplier_4(float value)
	{
		___AngleMultiplier_4 = value;
	}

	inline static int32_t get_offset_of_SpeedMultiplier_5() { return static_cast<int32_t>(offsetof(VertexShakeA_tCBD5B7397DD332038CE795E6B0A4D5B199739C27, ___SpeedMultiplier_5)); }
	inline float get_SpeedMultiplier_5() const { return ___SpeedMultiplier_5; }
	inline float* get_address_of_SpeedMultiplier_5() { return &___SpeedMultiplier_5; }
	inline void set_SpeedMultiplier_5(float value)
	{
		___SpeedMultiplier_5 = value;
	}

	inline static int32_t get_offset_of_ScaleMultiplier_6() { return static_cast<int32_t>(offsetof(VertexShakeA_tCBD5B7397DD332038CE795E6B0A4D5B199739C27, ___ScaleMultiplier_6)); }
	inline float get_ScaleMultiplier_6() const { return ___ScaleMultiplier_6; }
	inline float* get_address_of_ScaleMultiplier_6() { return &___ScaleMultiplier_6; }
	inline void set_ScaleMultiplier_6(float value)
	{
		___ScaleMultiplier_6 = value;
	}

	inline static int32_t get_offset_of_RotationMultiplier_7() { return static_cast<int32_t>(offsetof(VertexShakeA_tCBD5B7397DD332038CE795E6B0A4D5B199739C27, ___RotationMultiplier_7)); }
	inline float get_RotationMultiplier_7() const { return ___RotationMultiplier_7; }
	inline float* get_address_of_RotationMultiplier_7() { return &___RotationMultiplier_7; }
	inline void set_RotationMultiplier_7(float value)
	{
		___RotationMultiplier_7 = value;
	}

	inline static int32_t get_offset_of_m_TextComponent_8() { return static_cast<int32_t>(offsetof(VertexShakeA_tCBD5B7397DD332038CE795E6B0A4D5B199739C27, ___m_TextComponent_8)); }
	inline TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * get_m_TextComponent_8() const { return ___m_TextComponent_8; }
	inline TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 ** get_address_of_m_TextComponent_8() { return &___m_TextComponent_8; }
	inline void set_m_TextComponent_8(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * value)
	{
		___m_TextComponent_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextComponent_8), value);
	}

	inline static int32_t get_offset_of_hasTextChanged_9() { return static_cast<int32_t>(offsetof(VertexShakeA_tCBD5B7397DD332038CE795E6B0A4D5B199739C27, ___hasTextChanged_9)); }
	inline bool get_hasTextChanged_9() const { return ___hasTextChanged_9; }
	inline bool* get_address_of_hasTextChanged_9() { return &___hasTextChanged_9; }
	inline void set_hasTextChanged_9(bool value)
	{
		___hasTextChanged_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTEXSHAKEA_TCBD5B7397DD332038CE795E6B0A4D5B199739C27_H
#ifndef VERTEXSHAKEB_TC2BBE39E80035E065C9ADB888AA52006D10EDC0E_H
#define VERTEXSHAKEB_TC2BBE39E80035E065C9ADB888AA52006D10EDC0E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.VertexShakeB
struct  VertexShakeB_tC2BBE39E80035E065C9ADB888AA52006D10EDC0E  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single TMPro.Examples.VertexShakeB::AngleMultiplier
	float ___AngleMultiplier_4;
	// System.Single TMPro.Examples.VertexShakeB::SpeedMultiplier
	float ___SpeedMultiplier_5;
	// System.Single TMPro.Examples.VertexShakeB::CurveScale
	float ___CurveScale_6;
	// TMPro.TMP_Text TMPro.Examples.VertexShakeB::m_TextComponent
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * ___m_TextComponent_7;
	// System.Boolean TMPro.Examples.VertexShakeB::hasTextChanged
	bool ___hasTextChanged_8;

public:
	inline static int32_t get_offset_of_AngleMultiplier_4() { return static_cast<int32_t>(offsetof(VertexShakeB_tC2BBE39E80035E065C9ADB888AA52006D10EDC0E, ___AngleMultiplier_4)); }
	inline float get_AngleMultiplier_4() const { return ___AngleMultiplier_4; }
	inline float* get_address_of_AngleMultiplier_4() { return &___AngleMultiplier_4; }
	inline void set_AngleMultiplier_4(float value)
	{
		___AngleMultiplier_4 = value;
	}

	inline static int32_t get_offset_of_SpeedMultiplier_5() { return static_cast<int32_t>(offsetof(VertexShakeB_tC2BBE39E80035E065C9ADB888AA52006D10EDC0E, ___SpeedMultiplier_5)); }
	inline float get_SpeedMultiplier_5() const { return ___SpeedMultiplier_5; }
	inline float* get_address_of_SpeedMultiplier_5() { return &___SpeedMultiplier_5; }
	inline void set_SpeedMultiplier_5(float value)
	{
		___SpeedMultiplier_5 = value;
	}

	inline static int32_t get_offset_of_CurveScale_6() { return static_cast<int32_t>(offsetof(VertexShakeB_tC2BBE39E80035E065C9ADB888AA52006D10EDC0E, ___CurveScale_6)); }
	inline float get_CurveScale_6() const { return ___CurveScale_6; }
	inline float* get_address_of_CurveScale_6() { return &___CurveScale_6; }
	inline void set_CurveScale_6(float value)
	{
		___CurveScale_6 = value;
	}

	inline static int32_t get_offset_of_m_TextComponent_7() { return static_cast<int32_t>(offsetof(VertexShakeB_tC2BBE39E80035E065C9ADB888AA52006D10EDC0E, ___m_TextComponent_7)); }
	inline TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * get_m_TextComponent_7() const { return ___m_TextComponent_7; }
	inline TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 ** get_address_of_m_TextComponent_7() { return &___m_TextComponent_7; }
	inline void set_m_TextComponent_7(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * value)
	{
		___m_TextComponent_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextComponent_7), value);
	}

	inline static int32_t get_offset_of_hasTextChanged_8() { return static_cast<int32_t>(offsetof(VertexShakeB_tC2BBE39E80035E065C9ADB888AA52006D10EDC0E, ___hasTextChanged_8)); }
	inline bool get_hasTextChanged_8() const { return ___hasTextChanged_8; }
	inline bool* get_address_of_hasTextChanged_8() { return &___hasTextChanged_8; }
	inline void set_hasTextChanged_8(bool value)
	{
		___hasTextChanged_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTEXSHAKEB_TC2BBE39E80035E065C9ADB888AA52006D10EDC0E_H
#ifndef VERTEXZOOM_T78EE9E8C38B64E198806ADD562E22F722A2EE156_H
#define VERTEXZOOM_T78EE9E8C38B64E198806ADD562E22F722A2EE156_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.VertexZoom
struct  VertexZoom_t78EE9E8C38B64E198806ADD562E22F722A2EE156  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single TMPro.Examples.VertexZoom::AngleMultiplier
	float ___AngleMultiplier_4;
	// System.Single TMPro.Examples.VertexZoom::SpeedMultiplier
	float ___SpeedMultiplier_5;
	// System.Single TMPro.Examples.VertexZoom::CurveScale
	float ___CurveScale_6;
	// TMPro.TMP_Text TMPro.Examples.VertexZoom::m_TextComponent
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * ___m_TextComponent_7;
	// System.Boolean TMPro.Examples.VertexZoom::hasTextChanged
	bool ___hasTextChanged_8;

public:
	inline static int32_t get_offset_of_AngleMultiplier_4() { return static_cast<int32_t>(offsetof(VertexZoom_t78EE9E8C38B64E198806ADD562E22F722A2EE156, ___AngleMultiplier_4)); }
	inline float get_AngleMultiplier_4() const { return ___AngleMultiplier_4; }
	inline float* get_address_of_AngleMultiplier_4() { return &___AngleMultiplier_4; }
	inline void set_AngleMultiplier_4(float value)
	{
		___AngleMultiplier_4 = value;
	}

	inline static int32_t get_offset_of_SpeedMultiplier_5() { return static_cast<int32_t>(offsetof(VertexZoom_t78EE9E8C38B64E198806ADD562E22F722A2EE156, ___SpeedMultiplier_5)); }
	inline float get_SpeedMultiplier_5() const { return ___SpeedMultiplier_5; }
	inline float* get_address_of_SpeedMultiplier_5() { return &___SpeedMultiplier_5; }
	inline void set_SpeedMultiplier_5(float value)
	{
		___SpeedMultiplier_5 = value;
	}

	inline static int32_t get_offset_of_CurveScale_6() { return static_cast<int32_t>(offsetof(VertexZoom_t78EE9E8C38B64E198806ADD562E22F722A2EE156, ___CurveScale_6)); }
	inline float get_CurveScale_6() const { return ___CurveScale_6; }
	inline float* get_address_of_CurveScale_6() { return &___CurveScale_6; }
	inline void set_CurveScale_6(float value)
	{
		___CurveScale_6 = value;
	}

	inline static int32_t get_offset_of_m_TextComponent_7() { return static_cast<int32_t>(offsetof(VertexZoom_t78EE9E8C38B64E198806ADD562E22F722A2EE156, ___m_TextComponent_7)); }
	inline TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * get_m_TextComponent_7() const { return ___m_TextComponent_7; }
	inline TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 ** get_address_of_m_TextComponent_7() { return &___m_TextComponent_7; }
	inline void set_m_TextComponent_7(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * value)
	{
		___m_TextComponent_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextComponent_7), value);
	}

	inline static int32_t get_offset_of_hasTextChanged_8() { return static_cast<int32_t>(offsetof(VertexZoom_t78EE9E8C38B64E198806ADD562E22F722A2EE156, ___hasTextChanged_8)); }
	inline bool get_hasTextChanged_8() const { return ___hasTextChanged_8; }
	inline bool* get_address_of_hasTextChanged_8() { return &___hasTextChanged_8; }
	inline void set_hasTextChanged_8(bool value)
	{
		___hasTextChanged_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTEXZOOM_T78EE9E8C38B64E198806ADD562E22F722A2EE156_H
#ifndef WARPTEXTEXAMPLE_T918FAC4E8BDAF99FAE70218E452EB4EF81A1C9F2_H
#define WARPTEXTEXAMPLE_T918FAC4E8BDAF99FAE70218E452EB4EF81A1C9F2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.WarpTextExample
struct  WarpTextExample_t918FAC4E8BDAF99FAE70218E452EB4EF81A1C9F2  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// TMPro.TMP_Text TMPro.Examples.WarpTextExample::m_TextComponent
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * ___m_TextComponent_4;
	// UnityEngine.AnimationCurve TMPro.Examples.WarpTextExample::VertexCurve
	AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * ___VertexCurve_5;
	// System.Single TMPro.Examples.WarpTextExample::AngleMultiplier
	float ___AngleMultiplier_6;
	// System.Single TMPro.Examples.WarpTextExample::SpeedMultiplier
	float ___SpeedMultiplier_7;
	// System.Single TMPro.Examples.WarpTextExample::CurveScale
	float ___CurveScale_8;

public:
	inline static int32_t get_offset_of_m_TextComponent_4() { return static_cast<int32_t>(offsetof(WarpTextExample_t918FAC4E8BDAF99FAE70218E452EB4EF81A1C9F2, ___m_TextComponent_4)); }
	inline TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * get_m_TextComponent_4() const { return ___m_TextComponent_4; }
	inline TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 ** get_address_of_m_TextComponent_4() { return &___m_TextComponent_4; }
	inline void set_m_TextComponent_4(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * value)
	{
		___m_TextComponent_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextComponent_4), value);
	}

	inline static int32_t get_offset_of_VertexCurve_5() { return static_cast<int32_t>(offsetof(WarpTextExample_t918FAC4E8BDAF99FAE70218E452EB4EF81A1C9F2, ___VertexCurve_5)); }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * get_VertexCurve_5() const { return ___VertexCurve_5; }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C ** get_address_of_VertexCurve_5() { return &___VertexCurve_5; }
	inline void set_VertexCurve_5(AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * value)
	{
		___VertexCurve_5 = value;
		Il2CppCodeGenWriteBarrier((&___VertexCurve_5), value);
	}

	inline static int32_t get_offset_of_AngleMultiplier_6() { return static_cast<int32_t>(offsetof(WarpTextExample_t918FAC4E8BDAF99FAE70218E452EB4EF81A1C9F2, ___AngleMultiplier_6)); }
	inline float get_AngleMultiplier_6() const { return ___AngleMultiplier_6; }
	inline float* get_address_of_AngleMultiplier_6() { return &___AngleMultiplier_6; }
	inline void set_AngleMultiplier_6(float value)
	{
		___AngleMultiplier_6 = value;
	}

	inline static int32_t get_offset_of_SpeedMultiplier_7() { return static_cast<int32_t>(offsetof(WarpTextExample_t918FAC4E8BDAF99FAE70218E452EB4EF81A1C9F2, ___SpeedMultiplier_7)); }
	inline float get_SpeedMultiplier_7() const { return ___SpeedMultiplier_7; }
	inline float* get_address_of_SpeedMultiplier_7() { return &___SpeedMultiplier_7; }
	inline void set_SpeedMultiplier_7(float value)
	{
		___SpeedMultiplier_7 = value;
	}

	inline static int32_t get_offset_of_CurveScale_8() { return static_cast<int32_t>(offsetof(WarpTextExample_t918FAC4E8BDAF99FAE70218E452EB4EF81A1C9F2, ___CurveScale_8)); }
	inline float get_CurveScale_8() const { return ___CurveScale_8; }
	inline float* get_address_of_CurveScale_8() { return &___CurveScale_8; }
	inline void set_CurveScale_8(float value)
	{
		___CurveScale_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WARPTEXTEXAMPLE_T918FAC4E8BDAF99FAE70218E452EB4EF81A1C9F2_H
#ifndef TMP_TEXTEVENTHANDLER_T6046295E0F06C5138916541DBE6B476E6FE66B54_H
#define TMP_TEXTEVENTHANDLER_T6046295E0F06C5138916541DBE6B476E6FE66B54_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_TextEventHandler
struct  TMP_TextEventHandler_t6046295E0F06C5138916541DBE6B476E6FE66B54  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// TMPro.TMP_TextEventHandler/CharacterSelectionEvent TMPro.TMP_TextEventHandler::m_OnCharacterSelection
	CharacterSelectionEvent_t346DE6835B0DF86A32928016EE2C413E919DBF90 * ___m_OnCharacterSelection_4;
	// TMPro.TMP_TextEventHandler/SpriteSelectionEvent TMPro.TMP_TextEventHandler::m_OnSpriteSelection
	SpriteSelectionEvent_tCE8FEB1D487ED84CBA38BC47F8949C5537672B32 * ___m_OnSpriteSelection_5;
	// TMPro.TMP_TextEventHandler/WordSelectionEvent TMPro.TMP_TextEventHandler::m_OnWordSelection
	WordSelectionEvent_tF37108E717AF8FCEB63C4B4CB11231A5F030A554 * ___m_OnWordSelection_6;
	// TMPro.TMP_TextEventHandler/LineSelectionEvent TMPro.TMP_TextEventHandler::m_OnLineSelection
	LineSelectionEvent_t9F223C373E9EF88E12570D86DE2651F8EDEEAB3F * ___m_OnLineSelection_7;
	// TMPro.TMP_TextEventHandler/LinkSelectionEvent TMPro.TMP_TextEventHandler::m_OnLinkSelection
	LinkSelectionEvent_t61E2583194386CC362B13606ECE2F840876A24E8 * ___m_OnLinkSelection_8;
	// TMPro.TMP_Text TMPro.TMP_TextEventHandler::m_TextComponent
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * ___m_TextComponent_9;
	// UnityEngine.Camera TMPro.TMP_TextEventHandler::m_Camera
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___m_Camera_10;
	// UnityEngine.Canvas TMPro.TMP_TextEventHandler::m_Canvas
	Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * ___m_Canvas_11;
	// System.Int32 TMPro.TMP_TextEventHandler::m_selectedLink
	int32_t ___m_selectedLink_12;
	// System.Int32 TMPro.TMP_TextEventHandler::m_lastCharIndex
	int32_t ___m_lastCharIndex_13;
	// System.Int32 TMPro.TMP_TextEventHandler::m_lastWordIndex
	int32_t ___m_lastWordIndex_14;
	// System.Int32 TMPro.TMP_TextEventHandler::m_lastLineIndex
	int32_t ___m_lastLineIndex_15;

public:
	inline static int32_t get_offset_of_m_OnCharacterSelection_4() { return static_cast<int32_t>(offsetof(TMP_TextEventHandler_t6046295E0F06C5138916541DBE6B476E6FE66B54, ___m_OnCharacterSelection_4)); }
	inline CharacterSelectionEvent_t346DE6835B0DF86A32928016EE2C413E919DBF90 * get_m_OnCharacterSelection_4() const { return ___m_OnCharacterSelection_4; }
	inline CharacterSelectionEvent_t346DE6835B0DF86A32928016EE2C413E919DBF90 ** get_address_of_m_OnCharacterSelection_4() { return &___m_OnCharacterSelection_4; }
	inline void set_m_OnCharacterSelection_4(CharacterSelectionEvent_t346DE6835B0DF86A32928016EE2C413E919DBF90 * value)
	{
		___m_OnCharacterSelection_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnCharacterSelection_4), value);
	}

	inline static int32_t get_offset_of_m_OnSpriteSelection_5() { return static_cast<int32_t>(offsetof(TMP_TextEventHandler_t6046295E0F06C5138916541DBE6B476E6FE66B54, ___m_OnSpriteSelection_5)); }
	inline SpriteSelectionEvent_tCE8FEB1D487ED84CBA38BC47F8949C5537672B32 * get_m_OnSpriteSelection_5() const { return ___m_OnSpriteSelection_5; }
	inline SpriteSelectionEvent_tCE8FEB1D487ED84CBA38BC47F8949C5537672B32 ** get_address_of_m_OnSpriteSelection_5() { return &___m_OnSpriteSelection_5; }
	inline void set_m_OnSpriteSelection_5(SpriteSelectionEvent_tCE8FEB1D487ED84CBA38BC47F8949C5537672B32 * value)
	{
		___m_OnSpriteSelection_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnSpriteSelection_5), value);
	}

	inline static int32_t get_offset_of_m_OnWordSelection_6() { return static_cast<int32_t>(offsetof(TMP_TextEventHandler_t6046295E0F06C5138916541DBE6B476E6FE66B54, ___m_OnWordSelection_6)); }
	inline WordSelectionEvent_tF37108E717AF8FCEB63C4B4CB11231A5F030A554 * get_m_OnWordSelection_6() const { return ___m_OnWordSelection_6; }
	inline WordSelectionEvent_tF37108E717AF8FCEB63C4B4CB11231A5F030A554 ** get_address_of_m_OnWordSelection_6() { return &___m_OnWordSelection_6; }
	inline void set_m_OnWordSelection_6(WordSelectionEvent_tF37108E717AF8FCEB63C4B4CB11231A5F030A554 * value)
	{
		___m_OnWordSelection_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnWordSelection_6), value);
	}

	inline static int32_t get_offset_of_m_OnLineSelection_7() { return static_cast<int32_t>(offsetof(TMP_TextEventHandler_t6046295E0F06C5138916541DBE6B476E6FE66B54, ___m_OnLineSelection_7)); }
	inline LineSelectionEvent_t9F223C373E9EF88E12570D86DE2651F8EDEEAB3F * get_m_OnLineSelection_7() const { return ___m_OnLineSelection_7; }
	inline LineSelectionEvent_t9F223C373E9EF88E12570D86DE2651F8EDEEAB3F ** get_address_of_m_OnLineSelection_7() { return &___m_OnLineSelection_7; }
	inline void set_m_OnLineSelection_7(LineSelectionEvent_t9F223C373E9EF88E12570D86DE2651F8EDEEAB3F * value)
	{
		___m_OnLineSelection_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnLineSelection_7), value);
	}

	inline static int32_t get_offset_of_m_OnLinkSelection_8() { return static_cast<int32_t>(offsetof(TMP_TextEventHandler_t6046295E0F06C5138916541DBE6B476E6FE66B54, ___m_OnLinkSelection_8)); }
	inline LinkSelectionEvent_t61E2583194386CC362B13606ECE2F840876A24E8 * get_m_OnLinkSelection_8() const { return ___m_OnLinkSelection_8; }
	inline LinkSelectionEvent_t61E2583194386CC362B13606ECE2F840876A24E8 ** get_address_of_m_OnLinkSelection_8() { return &___m_OnLinkSelection_8; }
	inline void set_m_OnLinkSelection_8(LinkSelectionEvent_t61E2583194386CC362B13606ECE2F840876A24E8 * value)
	{
		___m_OnLinkSelection_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnLinkSelection_8), value);
	}

	inline static int32_t get_offset_of_m_TextComponent_9() { return static_cast<int32_t>(offsetof(TMP_TextEventHandler_t6046295E0F06C5138916541DBE6B476E6FE66B54, ___m_TextComponent_9)); }
	inline TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * get_m_TextComponent_9() const { return ___m_TextComponent_9; }
	inline TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 ** get_address_of_m_TextComponent_9() { return &___m_TextComponent_9; }
	inline void set_m_TextComponent_9(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * value)
	{
		___m_TextComponent_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextComponent_9), value);
	}

	inline static int32_t get_offset_of_m_Camera_10() { return static_cast<int32_t>(offsetof(TMP_TextEventHandler_t6046295E0F06C5138916541DBE6B476E6FE66B54, ___m_Camera_10)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get_m_Camera_10() const { return ___m_Camera_10; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of_m_Camera_10() { return &___m_Camera_10; }
	inline void set_m_Camera_10(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		___m_Camera_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_Camera_10), value);
	}

	inline static int32_t get_offset_of_m_Canvas_11() { return static_cast<int32_t>(offsetof(TMP_TextEventHandler_t6046295E0F06C5138916541DBE6B476E6FE66B54, ___m_Canvas_11)); }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * get_m_Canvas_11() const { return ___m_Canvas_11; }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 ** get_address_of_m_Canvas_11() { return &___m_Canvas_11; }
	inline void set_m_Canvas_11(Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * value)
	{
		___m_Canvas_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_Canvas_11), value);
	}

	inline static int32_t get_offset_of_m_selectedLink_12() { return static_cast<int32_t>(offsetof(TMP_TextEventHandler_t6046295E0F06C5138916541DBE6B476E6FE66B54, ___m_selectedLink_12)); }
	inline int32_t get_m_selectedLink_12() const { return ___m_selectedLink_12; }
	inline int32_t* get_address_of_m_selectedLink_12() { return &___m_selectedLink_12; }
	inline void set_m_selectedLink_12(int32_t value)
	{
		___m_selectedLink_12 = value;
	}

	inline static int32_t get_offset_of_m_lastCharIndex_13() { return static_cast<int32_t>(offsetof(TMP_TextEventHandler_t6046295E0F06C5138916541DBE6B476E6FE66B54, ___m_lastCharIndex_13)); }
	inline int32_t get_m_lastCharIndex_13() const { return ___m_lastCharIndex_13; }
	inline int32_t* get_address_of_m_lastCharIndex_13() { return &___m_lastCharIndex_13; }
	inline void set_m_lastCharIndex_13(int32_t value)
	{
		___m_lastCharIndex_13 = value;
	}

	inline static int32_t get_offset_of_m_lastWordIndex_14() { return static_cast<int32_t>(offsetof(TMP_TextEventHandler_t6046295E0F06C5138916541DBE6B476E6FE66B54, ___m_lastWordIndex_14)); }
	inline int32_t get_m_lastWordIndex_14() const { return ___m_lastWordIndex_14; }
	inline int32_t* get_address_of_m_lastWordIndex_14() { return &___m_lastWordIndex_14; }
	inline void set_m_lastWordIndex_14(int32_t value)
	{
		___m_lastWordIndex_14 = value;
	}

	inline static int32_t get_offset_of_m_lastLineIndex_15() { return static_cast<int32_t>(offsetof(TMP_TextEventHandler_t6046295E0F06C5138916541DBE6B476E6FE66B54, ___m_lastLineIndex_15)); }
	inline int32_t get_m_lastLineIndex_15() const { return ___m_lastLineIndex_15; }
	inline int32_t* get_address_of_m_lastLineIndex_15() { return &___m_lastLineIndex_15; }
	inline void set_m_lastLineIndex_15(int32_t value)
	{
		___m_lastLineIndex_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_TEXTEVENTHANDLER_T6046295E0F06C5138916541DBE6B476E6FE66B54_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2300 = { sizeof (TextMeshSpawner_t7A50739A3F9750469C9F130607DA20D66CA22686), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2300[4] = 
{
	TextMeshSpawner_t7A50739A3F9750469C9F130607DA20D66CA22686::get_offset_of_SpawnType_4(),
	TextMeshSpawner_t7A50739A3F9750469C9F130607DA20D66CA22686::get_offset_of_NumberOfNPC_5(),
	TextMeshSpawner_t7A50739A3F9750469C9F130607DA20D66CA22686::get_offset_of_TheFont_6(),
	TextMeshSpawner_t7A50739A3F9750469C9F130607DA20D66CA22686::get_offset_of_floatingText_Script_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2301 = { sizeof (TMP_DigitValidator_tD53B3EF123D04F923055895ED56555317D239AB5), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2302 = { sizeof (TMP_ExampleScript_01_t4208AD096DD4FCE676E1EE1FA3F4DFD6341D81A8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2302[5] = 
{
	TMP_ExampleScript_01_t4208AD096DD4FCE676E1EE1FA3F4DFD6341D81A8::get_offset_of_ObjectType_4(),
	TMP_ExampleScript_01_t4208AD096DD4FCE676E1EE1FA3F4DFD6341D81A8::get_offset_of_isStatic_5(),
	TMP_ExampleScript_01_t4208AD096DD4FCE676E1EE1FA3F4DFD6341D81A8::get_offset_of_m_text_6(),
	0,
	TMP_ExampleScript_01_t4208AD096DD4FCE676E1EE1FA3F4DFD6341D81A8::get_offset_of_count_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2303 = { sizeof (objectType_t4A9206B564B1134F55E176CB1D6A831B7F2CD0BE)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2303[3] = 
{
	objectType_t4A9206B564B1134F55E176CB1D6A831B7F2CD0BE::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2304 = { sizeof (TMP_FrameRateCounter_t154205AC6610245B31CEF1C182FA4B4862D9D07F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2304[10] = 
{
	TMP_FrameRateCounter_t154205AC6610245B31CEF1C182FA4B4862D9D07F::get_offset_of_UpdateInterval_4(),
	TMP_FrameRateCounter_t154205AC6610245B31CEF1C182FA4B4862D9D07F::get_offset_of_m_LastInterval_5(),
	TMP_FrameRateCounter_t154205AC6610245B31CEF1C182FA4B4862D9D07F::get_offset_of_m_Frames_6(),
	TMP_FrameRateCounter_t154205AC6610245B31CEF1C182FA4B4862D9D07F::get_offset_of_AnchorPosition_7(),
	TMP_FrameRateCounter_t154205AC6610245B31CEF1C182FA4B4862D9D07F::get_offset_of_htmlColorTag_8(),
	0,
	TMP_FrameRateCounter_t154205AC6610245B31CEF1C182FA4B4862D9D07F::get_offset_of_m_TextMeshPro_10(),
	TMP_FrameRateCounter_t154205AC6610245B31CEF1C182FA4B4862D9D07F::get_offset_of_m_frameCounter_transform_11(),
	TMP_FrameRateCounter_t154205AC6610245B31CEF1C182FA4B4862D9D07F::get_offset_of_m_camera_12(),
	TMP_FrameRateCounter_t154205AC6610245B31CEF1C182FA4B4862D9D07F::get_offset_of_last_AnchorPosition_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2305 = { sizeof (FpsCounterAnchorPositions_tF97255C7539822AFCA3511F3C0C4E6803A11EA97)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2305[5] = 
{
	FpsCounterAnchorPositions_tF97255C7539822AFCA3511F3C0C4E6803A11EA97::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2306 = { sizeof (TMP_PhoneNumberValidator_t7EB41CFDB7C6AA586BF5AF04151FC2228F565BD2), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2307 = { sizeof (TMP_TextEventCheck_t5AF390C2FFBE0FBCFA4F905503A504F5DD891CAB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2307[1] = 
{
	TMP_TextEventCheck_t5AF390C2FFBE0FBCFA4F905503A504F5DD891CAB::get_offset_of_TextEventHandler_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2308 = { sizeof (TMP_TextEventHandler_t6046295E0F06C5138916541DBE6B476E6FE66B54), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2308[12] = 
{
	TMP_TextEventHandler_t6046295E0F06C5138916541DBE6B476E6FE66B54::get_offset_of_m_OnCharacterSelection_4(),
	TMP_TextEventHandler_t6046295E0F06C5138916541DBE6B476E6FE66B54::get_offset_of_m_OnSpriteSelection_5(),
	TMP_TextEventHandler_t6046295E0F06C5138916541DBE6B476E6FE66B54::get_offset_of_m_OnWordSelection_6(),
	TMP_TextEventHandler_t6046295E0F06C5138916541DBE6B476E6FE66B54::get_offset_of_m_OnLineSelection_7(),
	TMP_TextEventHandler_t6046295E0F06C5138916541DBE6B476E6FE66B54::get_offset_of_m_OnLinkSelection_8(),
	TMP_TextEventHandler_t6046295E0F06C5138916541DBE6B476E6FE66B54::get_offset_of_m_TextComponent_9(),
	TMP_TextEventHandler_t6046295E0F06C5138916541DBE6B476E6FE66B54::get_offset_of_m_Camera_10(),
	TMP_TextEventHandler_t6046295E0F06C5138916541DBE6B476E6FE66B54::get_offset_of_m_Canvas_11(),
	TMP_TextEventHandler_t6046295E0F06C5138916541DBE6B476E6FE66B54::get_offset_of_m_selectedLink_12(),
	TMP_TextEventHandler_t6046295E0F06C5138916541DBE6B476E6FE66B54::get_offset_of_m_lastCharIndex_13(),
	TMP_TextEventHandler_t6046295E0F06C5138916541DBE6B476E6FE66B54::get_offset_of_m_lastWordIndex_14(),
	TMP_TextEventHandler_t6046295E0F06C5138916541DBE6B476E6FE66B54::get_offset_of_m_lastLineIndex_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2309 = { sizeof (CharacterSelectionEvent_t346DE6835B0DF86A32928016EE2C413E919DBF90), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2310 = { sizeof (SpriteSelectionEvent_tCE8FEB1D487ED84CBA38BC47F8949C5537672B32), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2311 = { sizeof (WordSelectionEvent_tF37108E717AF8FCEB63C4B4CB11231A5F030A554), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2312 = { sizeof (LineSelectionEvent_t9F223C373E9EF88E12570D86DE2651F8EDEEAB3F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2313 = { sizeof (LinkSelectionEvent_t61E2583194386CC362B13606ECE2F840876A24E8), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2314 = { sizeof (TMP_TextInfoDebugTool_t327D46DA1CC9A18738A134F109EA5AD3F8437FE7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2314[9] = 
{
	TMP_TextInfoDebugTool_t327D46DA1CC9A18738A134F109EA5AD3F8437FE7::get_offset_of_ShowCharacters_4(),
	TMP_TextInfoDebugTool_t327D46DA1CC9A18738A134F109EA5AD3F8437FE7::get_offset_of_ShowWords_5(),
	TMP_TextInfoDebugTool_t327D46DA1CC9A18738A134F109EA5AD3F8437FE7::get_offset_of_ShowLinks_6(),
	TMP_TextInfoDebugTool_t327D46DA1CC9A18738A134F109EA5AD3F8437FE7::get_offset_of_ShowLines_7(),
	TMP_TextInfoDebugTool_t327D46DA1CC9A18738A134F109EA5AD3F8437FE7::get_offset_of_ShowMeshBounds_8(),
	TMP_TextInfoDebugTool_t327D46DA1CC9A18738A134F109EA5AD3F8437FE7::get_offset_of_ShowTextBounds_9(),
	TMP_TextInfoDebugTool_t327D46DA1CC9A18738A134F109EA5AD3F8437FE7::get_offset_of_ObjectStats_10(),
	TMP_TextInfoDebugTool_t327D46DA1CC9A18738A134F109EA5AD3F8437FE7::get_offset_of_m_TextComponent_11(),
	TMP_TextInfoDebugTool_t327D46DA1CC9A18738A134F109EA5AD3F8437FE7::get_offset_of_m_Transform_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2315 = { sizeof (TMP_TextSelector_A_t1931459985F680024B2C3E2E27919BE367243C6E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2315[6] = 
{
	TMP_TextSelector_A_t1931459985F680024B2C3E2E27919BE367243C6E::get_offset_of_m_TextMeshPro_4(),
	TMP_TextSelector_A_t1931459985F680024B2C3E2E27919BE367243C6E::get_offset_of_m_Camera_5(),
	TMP_TextSelector_A_t1931459985F680024B2C3E2E27919BE367243C6E::get_offset_of_m_isHoveringObject_6(),
	TMP_TextSelector_A_t1931459985F680024B2C3E2E27919BE367243C6E::get_offset_of_m_selectedLink_7(),
	TMP_TextSelector_A_t1931459985F680024B2C3E2E27919BE367243C6E::get_offset_of_m_lastCharIndex_8(),
	TMP_TextSelector_A_t1931459985F680024B2C3E2E27919BE367243C6E::get_offset_of_m_lastWordIndex_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2316 = { sizeof (TMP_TextSelector_B_t9BD007A30F11CFDAB04DFD4FA6769209EC8BF44B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2316[14] = 
{
	TMP_TextSelector_B_t9BD007A30F11CFDAB04DFD4FA6769209EC8BF44B::get_offset_of_TextPopup_Prefab_01_4(),
	TMP_TextSelector_B_t9BD007A30F11CFDAB04DFD4FA6769209EC8BF44B::get_offset_of_m_TextPopup_RectTransform_5(),
	TMP_TextSelector_B_t9BD007A30F11CFDAB04DFD4FA6769209EC8BF44B::get_offset_of_m_TextPopup_TMPComponent_6(),
	0,
	0,
	TMP_TextSelector_B_t9BD007A30F11CFDAB04DFD4FA6769209EC8BF44B::get_offset_of_m_TextMeshPro_9(),
	TMP_TextSelector_B_t9BD007A30F11CFDAB04DFD4FA6769209EC8BF44B::get_offset_of_m_Canvas_10(),
	TMP_TextSelector_B_t9BD007A30F11CFDAB04DFD4FA6769209EC8BF44B::get_offset_of_m_Camera_11(),
	TMP_TextSelector_B_t9BD007A30F11CFDAB04DFD4FA6769209EC8BF44B::get_offset_of_isHoveringObject_12(),
	TMP_TextSelector_B_t9BD007A30F11CFDAB04DFD4FA6769209EC8BF44B::get_offset_of_m_selectedWord_13(),
	TMP_TextSelector_B_t9BD007A30F11CFDAB04DFD4FA6769209EC8BF44B::get_offset_of_m_selectedLink_14(),
	TMP_TextSelector_B_t9BD007A30F11CFDAB04DFD4FA6769209EC8BF44B::get_offset_of_m_lastIndex_15(),
	TMP_TextSelector_B_t9BD007A30F11CFDAB04DFD4FA6769209EC8BF44B::get_offset_of_m_matrix_16(),
	TMP_TextSelector_B_t9BD007A30F11CFDAB04DFD4FA6769209EC8BF44B::get_offset_of_m_cachedMeshInfoVertexData_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2317 = { sizeof (TMP_UiFrameRateCounter_tA4B70430361CC81F922192640261ACA474A98C20), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2317[9] = 
{
	TMP_UiFrameRateCounter_tA4B70430361CC81F922192640261ACA474A98C20::get_offset_of_UpdateInterval_4(),
	TMP_UiFrameRateCounter_tA4B70430361CC81F922192640261ACA474A98C20::get_offset_of_m_LastInterval_5(),
	TMP_UiFrameRateCounter_tA4B70430361CC81F922192640261ACA474A98C20::get_offset_of_m_Frames_6(),
	TMP_UiFrameRateCounter_tA4B70430361CC81F922192640261ACA474A98C20::get_offset_of_AnchorPosition_7(),
	TMP_UiFrameRateCounter_tA4B70430361CC81F922192640261ACA474A98C20::get_offset_of_htmlColorTag_8(),
	0,
	TMP_UiFrameRateCounter_tA4B70430361CC81F922192640261ACA474A98C20::get_offset_of_m_TextMeshPro_10(),
	TMP_UiFrameRateCounter_tA4B70430361CC81F922192640261ACA474A98C20::get_offset_of_m_frameCounter_transform_11(),
	TMP_UiFrameRateCounter_tA4B70430361CC81F922192640261ACA474A98C20::get_offset_of_last_AnchorPosition_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2318 = { sizeof (FpsCounterAnchorPositions_t4FFCAA8E98FD34C931C6524BB63FBE9828B9FBD0)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2318[5] = 
{
	FpsCounterAnchorPositions_t4FFCAA8E98FD34C931C6524BB63FBE9828B9FBD0::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2319 = { sizeof (TMPro_InstructionOverlay_t0951D0394E58954E16D604924BD2FD0110A3E31E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2319[6] = 
{
	TMPro_InstructionOverlay_t0951D0394E58954E16D604924BD2FD0110A3E31E::get_offset_of_AnchorPosition_4(),
	0,
	TMPro_InstructionOverlay_t0951D0394E58954E16D604924BD2FD0110A3E31E::get_offset_of_m_TextMeshPro_6(),
	TMPro_InstructionOverlay_t0951D0394E58954E16D604924BD2FD0110A3E31E::get_offset_of_m_textContainer_7(),
	TMPro_InstructionOverlay_t0951D0394E58954E16D604924BD2FD0110A3E31E::get_offset_of_m_frameCounter_transform_8(),
	TMPro_InstructionOverlay_t0951D0394E58954E16D604924BD2FD0110A3E31E::get_offset_of_m_camera_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2320 = { sizeof (FpsCounterAnchorPositions_tB558896962FD344179F43905F951BF8A256B8642)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2320[5] = 
{
	FpsCounterAnchorPositions_tB558896962FD344179F43905F951BF8A256B8642::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2321 = { sizeof (VertexColorCycler_t5EEC79CC79AA4B00DF4BE5CE390C12E47385299C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2321[1] = 
{
	VertexColorCycler_t5EEC79CC79AA4B00DF4BE5CE390C12E47385299C::get_offset_of_m_TextComponent_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2322 = { sizeof (U3CAnimateVertexColorsU3Ec__Iterator0_t7BBBF1D9BED81210183303D57EA4AEE43847D4BA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2322[11] = 
{
	U3CAnimateVertexColorsU3Ec__Iterator0_t7BBBF1D9BED81210183303D57EA4AEE43847D4BA::get_offset_of_U3CtextInfoU3E__0_0(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t7BBBF1D9BED81210183303D57EA4AEE43847D4BA::get_offset_of_U3CcurrentCharacterU3E__0_1(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t7BBBF1D9BED81210183303D57EA4AEE43847D4BA::get_offset_of_U3Cc0U3E__0_2(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t7BBBF1D9BED81210183303D57EA4AEE43847D4BA::get_offset_of_U3CcharacterCountU3E__1_3(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t7BBBF1D9BED81210183303D57EA4AEE43847D4BA::get_offset_of_U3CmaterialIndexU3E__1_4(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t7BBBF1D9BED81210183303D57EA4AEE43847D4BA::get_offset_of_U3CnewVertexColorsU3E__1_5(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t7BBBF1D9BED81210183303D57EA4AEE43847D4BA::get_offset_of_U3CvertexIndexU3E__1_6(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t7BBBF1D9BED81210183303D57EA4AEE43847D4BA::get_offset_of_U24this_7(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t7BBBF1D9BED81210183303D57EA4AEE43847D4BA::get_offset_of_U24current_8(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t7BBBF1D9BED81210183303D57EA4AEE43847D4BA::get_offset_of_U24disposing_9(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t7BBBF1D9BED81210183303D57EA4AEE43847D4BA::get_offset_of_U24PC_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2323 = { sizeof (VertexJitter_tF4F1AE4AF51C09E6FE57F91227A228B61BD0ACF0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2323[5] = 
{
	VertexJitter_tF4F1AE4AF51C09E6FE57F91227A228B61BD0ACF0::get_offset_of_AngleMultiplier_4(),
	VertexJitter_tF4F1AE4AF51C09E6FE57F91227A228B61BD0ACF0::get_offset_of_SpeedMultiplier_5(),
	VertexJitter_tF4F1AE4AF51C09E6FE57F91227A228B61BD0ACF0::get_offset_of_CurveScale_6(),
	VertexJitter_tF4F1AE4AF51C09E6FE57F91227A228B61BD0ACF0::get_offset_of_m_TextComponent_7(),
	VertexJitter_tF4F1AE4AF51C09E6FE57F91227A228B61BD0ACF0::get_offset_of_hasTextChanged_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2324 = { sizeof (VertexAnim_t8FDDCAB8C8E4CDA915AACE07F8ACD16108BEA61C)+ sizeof (RuntimeObject), sizeof(VertexAnim_t8FDDCAB8C8E4CDA915AACE07F8ACD16108BEA61C ), 0, 0 };
extern const int32_t g_FieldOffsetTable2324[3] = 
{
	VertexAnim_t8FDDCAB8C8E4CDA915AACE07F8ACD16108BEA61C::get_offset_of_angleRange_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VertexAnim_t8FDDCAB8C8E4CDA915AACE07F8ACD16108BEA61C::get_offset_of_angle_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VertexAnim_t8FDDCAB8C8E4CDA915AACE07F8ACD16108BEA61C::get_offset_of_speed_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2325 = { sizeof (U3CAnimateVertexColorsU3Ec__Iterator0_tFA247E5BC602CE6265BA54F19C1C496C28DABE6D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2325[10] = 
{
	U3CAnimateVertexColorsU3Ec__Iterator0_tFA247E5BC602CE6265BA54F19C1C496C28DABE6D::get_offset_of_U3CtextInfoU3E__0_0(),
	U3CAnimateVertexColorsU3Ec__Iterator0_tFA247E5BC602CE6265BA54F19C1C496C28DABE6D::get_offset_of_U3CloopCountU3E__0_1(),
	U3CAnimateVertexColorsU3Ec__Iterator0_tFA247E5BC602CE6265BA54F19C1C496C28DABE6D::get_offset_of_U3CvertexAnimU3E__0_2(),
	U3CAnimateVertexColorsU3Ec__Iterator0_tFA247E5BC602CE6265BA54F19C1C496C28DABE6D::get_offset_of_U3CcachedMeshInfoU3E__0_3(),
	U3CAnimateVertexColorsU3Ec__Iterator0_tFA247E5BC602CE6265BA54F19C1C496C28DABE6D::get_offset_of_U3CcharacterCountU3E__1_4(),
	U3CAnimateVertexColorsU3Ec__Iterator0_tFA247E5BC602CE6265BA54F19C1C496C28DABE6D::get_offset_of_U3CmatrixU3E__2_5(),
	U3CAnimateVertexColorsU3Ec__Iterator0_tFA247E5BC602CE6265BA54F19C1C496C28DABE6D::get_offset_of_U24this_6(),
	U3CAnimateVertexColorsU3Ec__Iterator0_tFA247E5BC602CE6265BA54F19C1C496C28DABE6D::get_offset_of_U24current_7(),
	U3CAnimateVertexColorsU3Ec__Iterator0_tFA247E5BC602CE6265BA54F19C1C496C28DABE6D::get_offset_of_U24disposing_8(),
	U3CAnimateVertexColorsU3Ec__Iterator0_tFA247E5BC602CE6265BA54F19C1C496C28DABE6D::get_offset_of_U24PC_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2326 = { sizeof (VertexShakeA_tCBD5B7397DD332038CE795E6B0A4D5B199739C27), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2326[6] = 
{
	VertexShakeA_tCBD5B7397DD332038CE795E6B0A4D5B199739C27::get_offset_of_AngleMultiplier_4(),
	VertexShakeA_tCBD5B7397DD332038CE795E6B0A4D5B199739C27::get_offset_of_SpeedMultiplier_5(),
	VertexShakeA_tCBD5B7397DD332038CE795E6B0A4D5B199739C27::get_offset_of_ScaleMultiplier_6(),
	VertexShakeA_tCBD5B7397DD332038CE795E6B0A4D5B199739C27::get_offset_of_RotationMultiplier_7(),
	VertexShakeA_tCBD5B7397DD332038CE795E6B0A4D5B199739C27::get_offset_of_m_TextComponent_8(),
	VertexShakeA_tCBD5B7397DD332038CE795E6B0A4D5B199739C27::get_offset_of_hasTextChanged_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2327 = { sizeof (U3CAnimateVertexColorsU3Ec__Iterator0_t14AC1A090C4142BE438C54B95703A6AF8243626E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2327[9] = 
{
	U3CAnimateVertexColorsU3Ec__Iterator0_t14AC1A090C4142BE438C54B95703A6AF8243626E::get_offset_of_U3CtextInfoU3E__0_0(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t14AC1A090C4142BE438C54B95703A6AF8243626E::get_offset_of_U3CcopyOfVerticesU3E__0_1(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t14AC1A090C4142BE438C54B95703A6AF8243626E::get_offset_of_U3CcharacterCountU3E__1_2(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t14AC1A090C4142BE438C54B95703A6AF8243626E::get_offset_of_U3ClineCountU3E__1_3(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t14AC1A090C4142BE438C54B95703A6AF8243626E::get_offset_of_U3CmatrixU3E__2_4(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t14AC1A090C4142BE438C54B95703A6AF8243626E::get_offset_of_U24this_5(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t14AC1A090C4142BE438C54B95703A6AF8243626E::get_offset_of_U24current_6(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t14AC1A090C4142BE438C54B95703A6AF8243626E::get_offset_of_U24disposing_7(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t14AC1A090C4142BE438C54B95703A6AF8243626E::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2328 = { sizeof (VertexShakeB_tC2BBE39E80035E065C9ADB888AA52006D10EDC0E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2328[5] = 
{
	VertexShakeB_tC2BBE39E80035E065C9ADB888AA52006D10EDC0E::get_offset_of_AngleMultiplier_4(),
	VertexShakeB_tC2BBE39E80035E065C9ADB888AA52006D10EDC0E::get_offset_of_SpeedMultiplier_5(),
	VertexShakeB_tC2BBE39E80035E065C9ADB888AA52006D10EDC0E::get_offset_of_CurveScale_6(),
	VertexShakeB_tC2BBE39E80035E065C9ADB888AA52006D10EDC0E::get_offset_of_m_TextComponent_7(),
	VertexShakeB_tC2BBE39E80035E065C9ADB888AA52006D10EDC0E::get_offset_of_hasTextChanged_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2329 = { sizeof (U3CAnimateVertexColorsU3Ec__Iterator0_t7ECD09F2BEC8C31A570058712068497BDEAB23E4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2329[9] = 
{
	U3CAnimateVertexColorsU3Ec__Iterator0_t7ECD09F2BEC8C31A570058712068497BDEAB23E4::get_offset_of_U3CtextInfoU3E__0_0(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t7ECD09F2BEC8C31A570058712068497BDEAB23E4::get_offset_of_U3CcopyOfVerticesU3E__0_1(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t7ECD09F2BEC8C31A570058712068497BDEAB23E4::get_offset_of_U3CcharacterCountU3E__1_2(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t7ECD09F2BEC8C31A570058712068497BDEAB23E4::get_offset_of_U3ClineCountU3E__1_3(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t7ECD09F2BEC8C31A570058712068497BDEAB23E4::get_offset_of_U3CmatrixU3E__2_4(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t7ECD09F2BEC8C31A570058712068497BDEAB23E4::get_offset_of_U24this_5(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t7ECD09F2BEC8C31A570058712068497BDEAB23E4::get_offset_of_U24current_6(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t7ECD09F2BEC8C31A570058712068497BDEAB23E4::get_offset_of_U24disposing_7(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t7ECD09F2BEC8C31A570058712068497BDEAB23E4::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2330 = { sizeof (VertexZoom_t78EE9E8C38B64E198806ADD562E22F722A2EE156), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2330[5] = 
{
	VertexZoom_t78EE9E8C38B64E198806ADD562E22F722A2EE156::get_offset_of_AngleMultiplier_4(),
	VertexZoom_t78EE9E8C38B64E198806ADD562E22F722A2EE156::get_offset_of_SpeedMultiplier_5(),
	VertexZoom_t78EE9E8C38B64E198806ADD562E22F722A2EE156::get_offset_of_CurveScale_6(),
	VertexZoom_t78EE9E8C38B64E198806ADD562E22F722A2EE156::get_offset_of_m_TextComponent_7(),
	VertexZoom_t78EE9E8C38B64E198806ADD562E22F722A2EE156::get_offset_of_hasTextChanged_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2331 = { sizeof (U3CAnimateVertexColorsU3Ec__Iterator0_t4CE04A71641C727D7CFD708F629B27504B50CE7D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2331[10] = 
{
	U3CAnimateVertexColorsU3Ec__Iterator0_t4CE04A71641C727D7CFD708F629B27504B50CE7D::get_offset_of_U3CtextInfoU3E__0_0(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t4CE04A71641C727D7CFD708F629B27504B50CE7D::get_offset_of_U3CcachedMeshInfoVertexDataU3E__0_1(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t4CE04A71641C727D7CFD708F629B27504B50CE7D::get_offset_of_U3CscaleSortingOrderU3E__0_2(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t4CE04A71641C727D7CFD708F629B27504B50CE7D::get_offset_of_U3CcharacterCountU3E__1_3(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t4CE04A71641C727D7CFD708F629B27504B50CE7D::get_offset_of_U3CmatrixU3E__2_4(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t4CE04A71641C727D7CFD708F629B27504B50CE7D::get_offset_of_U24this_5(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t4CE04A71641C727D7CFD708F629B27504B50CE7D::get_offset_of_U24current_6(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t4CE04A71641C727D7CFD708F629B27504B50CE7D::get_offset_of_U24disposing_7(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t4CE04A71641C727D7CFD708F629B27504B50CE7D::get_offset_of_U24PC_8(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t4CE04A71641C727D7CFD708F629B27504B50CE7D::get_offset_of_U24locvar0_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2332 = { sizeof (U3CAnimateVertexColorsU3Ec__AnonStorey1_t22561508C8B53D4F45837DFC591B58212EEB80D6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2332[2] = 
{
	U3CAnimateVertexColorsU3Ec__AnonStorey1_t22561508C8B53D4F45837DFC591B58212EEB80D6::get_offset_of_modifiedCharScale_0(),
	U3CAnimateVertexColorsU3Ec__AnonStorey1_t22561508C8B53D4F45837DFC591B58212EEB80D6::get_offset_of_U3CU3Ef__refU240_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2333 = { sizeof (WarpTextExample_t918FAC4E8BDAF99FAE70218E452EB4EF81A1C9F2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2333[5] = 
{
	WarpTextExample_t918FAC4E8BDAF99FAE70218E452EB4EF81A1C9F2::get_offset_of_m_TextComponent_4(),
	WarpTextExample_t918FAC4E8BDAF99FAE70218E452EB4EF81A1C9F2::get_offset_of_VertexCurve_5(),
	WarpTextExample_t918FAC4E8BDAF99FAE70218E452EB4EF81A1C9F2::get_offset_of_AngleMultiplier_6(),
	WarpTextExample_t918FAC4E8BDAF99FAE70218E452EB4EF81A1C9F2::get_offset_of_SpeedMultiplier_7(),
	WarpTextExample_t918FAC4E8BDAF99FAE70218E452EB4EF81A1C9F2::get_offset_of_CurveScale_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2334 = { sizeof (U3CWarpTextU3Ec__Iterator0_tB564D42ED024F833E335B6B0EB07DCC2F7BC438A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2334[12] = 
{
	U3CWarpTextU3Ec__Iterator0_tB564D42ED024F833E335B6B0EB07DCC2F7BC438A::get_offset_of_U3Cold_CurveScaleU3E__0_0(),
	U3CWarpTextU3Ec__Iterator0_tB564D42ED024F833E335B6B0EB07DCC2F7BC438A::get_offset_of_U3Cold_curveU3E__0_1(),
	U3CWarpTextU3Ec__Iterator0_tB564D42ED024F833E335B6B0EB07DCC2F7BC438A::get_offset_of_U3CtextInfoU3E__1_2(),
	U3CWarpTextU3Ec__Iterator0_tB564D42ED024F833E335B6B0EB07DCC2F7BC438A::get_offset_of_U3CcharacterCountU3E__1_3(),
	U3CWarpTextU3Ec__Iterator0_tB564D42ED024F833E335B6B0EB07DCC2F7BC438A::get_offset_of_U3CboundsMinXU3E__1_4(),
	U3CWarpTextU3Ec__Iterator0_tB564D42ED024F833E335B6B0EB07DCC2F7BC438A::get_offset_of_U3CboundsMaxXU3E__1_5(),
	U3CWarpTextU3Ec__Iterator0_tB564D42ED024F833E335B6B0EB07DCC2F7BC438A::get_offset_of_U3CverticesU3E__2_6(),
	U3CWarpTextU3Ec__Iterator0_tB564D42ED024F833E335B6B0EB07DCC2F7BC438A::get_offset_of_U3CmatrixU3E__2_7(),
	U3CWarpTextU3Ec__Iterator0_tB564D42ED024F833E335B6B0EB07DCC2F7BC438A::get_offset_of_U24this_8(),
	U3CWarpTextU3Ec__Iterator0_tB564D42ED024F833E335B6B0EB07DCC2F7BC438A::get_offset_of_U24current_9(),
	U3CWarpTextU3Ec__Iterator0_tB564D42ED024F833E335B6B0EB07DCC2F7BC438A::get_offset_of_U24disposing_10(),
	U3CWarpTextU3Ec__Iterator0_tB564D42ED024F833E335B6B0EB07DCC2F7BC438A::get_offset_of_U24PC_11(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
